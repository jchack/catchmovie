package com.universe.catchmovie.dialog;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.universe.catchmovie.interfaces.ICustomAlertDialog;

public class CustomAlertDialog extends Dialog implements OnClickListener {
    public ICustomAlertDialog delegate;
    public String dialogID;
    public int requestID;
    public Object dialogTag;
    int[] layoutSubViews;
    private Activity activity;

    /**
     * @param layout         main_alert_dialog_layout
     * @param layoutSubViews id.alert_header_textView, id.alert_body_textView, id.alert_ok_button, id.alert_cancel_button,
     *                       id.alert_customview_layout
     */
    public CustomAlertDialog(Activity _activity,
                             String headText,
                             String bodyText,
                             boolean addCancelButton,
                             String cancelButtonText,
                             boolean addOkButton,
                             String okButtonText,
                             boolean cancelable,
                             int buttonsBackgroundResource,
                             View customView, int layout, int[] layoutSubViews) {
        super(_activity);

        this.activity = _activity;
        this.layoutSubViews = layoutSubViews;

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(layout, //R.layout.main_alert_dialog_layout,
                null, false);

        TextView headTextView;
        TextView bodyTextView;

        Button submitButton;
        Button cancelButton;

        LinearLayout customAlertView;

        if (view != null) {
            headTextView = (TextView) view.findViewById(layoutSubViews[0]);//R.id.alert_header_textView);
            bodyTextView = (TextView) view.findViewById(layoutSubViews[1]);//R.id.alert_body_textView);

            submitButton = (Button) view.findViewById(layoutSubViews[2]);//R.id.alert_ok_button);
            cancelButton = (Button) view.findViewById(layoutSubViews[3]);//R.id.alert_cancel_button);

            customAlertView = (LinearLayout) view.findViewById(layoutSubViews[4]);//R.id.alert_customview_layout);

            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.setContentView(view);
            this.setCancelable(cancelable);

            if (headText != null)
                headTextView.setText(headText);
            else
                headTextView.setVisibility(TextView.GONE);

            if (bodyText != null)
                bodyTextView.setText(bodyText);
            else
                bodyTextView.setVisibility(TextView.GONE);

            if (addCancelButton) {
                cancelButton.setOnClickListener(this);

                if (cancelButtonText != null && cancelButtonText.trim().length() > 0)
                    cancelButton.setText(cancelButtonText);

                if (buttonsBackgroundResource != 0)
                    cancelButton.setBackgroundResource(buttonsBackgroundResource);
            } else
                cancelButton.setVisibility(Button.GONE);

            if (addOkButton) {
                submitButton.setOnClickListener(this);

                if (okButtonText != null && okButtonText.trim().length() > 0)
                    submitButton.setText(okButtonText);

                if (buttonsBackgroundResource != 0)
                    submitButton.setBackgroundResource(buttonsBackgroundResource);
            } else
                submitButton.setVisibility(Button.GONE);

            if (customView != null) {
                customAlertView.setVisibility(LinearLayout.VISIBLE);
                customAlertView.addView(customView);
            }
        }
    }

    /**
     * @param layout         main_alert_dialog_layout
     * @param layoutSubViews id.alert_header_textView, id.alert_body_textView, id.alert_ok_button, id.alert_cancel_button,
     *                       id.alert_customview_layout
     */
    public CustomAlertDialog(Activity _activity,
                             String headText,
                             String bodyText,
                             boolean addCancelButton,
                             boolean addOkButton,
                             boolean cancelable,
                             View customView, int layout, int[] layoutSubViews) {
        super(_activity);

        this.activity = _activity;
        this.layoutSubViews = layoutSubViews;

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(layout, //R.layout.main_alert_dialog_layout,
                null, false);

        TextView headTextView;
        TextView bodyTextView;

        Button submitButton;
        Button cancelButton;

        LinearLayout customAlertView;

        if (view != null) {
            headTextView = (TextView) view.findViewById(layoutSubViews[0]);//R.id.alert_header_textView);
            bodyTextView = (TextView) view.findViewById(layoutSubViews[1]);//R.id.alert_body_textView);

            submitButton = (Button) view.findViewById(layoutSubViews[2]);//R.id.alert_ok_button);
            cancelButton = (Button) view.findViewById(layoutSubViews[3]);//R.id.alert_cancel_button);

            customAlertView = (LinearLayout) view.findViewById(layoutSubViews[4]);//R.id.alert_customview_layout);

            this.requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.setContentView(view);
            this.setCancelable(cancelable);

            if (headText != null)
                headTextView.setText(headText);
            else
                headTextView.setVisibility(TextView.GONE);

            if (bodyText != null)
                bodyTextView.setText(bodyText);
            else
                bodyTextView.setVisibility(TextView.GONE);

            if (addCancelButton)
                cancelButton.setOnClickListener(this);
            else
                cancelButton.setVisibility(Button.GONE);

            if (addOkButton)
                submitButton.setOnClickListener(this);
            else
                submitButton.setVisibility(Button.GONE);

            if (customView != null) {
                customAlertView.setVisibility(LinearLayout.VISIBLE);
                customAlertView.addView(customView);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == layoutSubViews[2])//R.id.alert_ok_button)
            delegate.onOkAlertClick(this, activity);
        else if (view.getId() == layoutSubViews[3])//R.id.alert_cancel_button)
            delegate.onCancelAlertClick(this, activity);
    }
}
