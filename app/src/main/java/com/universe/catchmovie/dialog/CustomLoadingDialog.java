package com.universe.catchmovie.dialog;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomLoadingDialog extends ProgressDialog {

    int layout;
    int[] layoutSubViews;
    private Context activity;

    /**
     * @param layout         loading_dialog_layout
     * @param layoutSubViews id.loading_imageView, id.loading_textView
     *                       <p/>
     *                       string.main_loading_label
     *                       <p/>
     *                       drawable.loading
     */
    public CustomLoadingDialog(Context context, int layout, int[] layoutSubViews) {
        super(context);

        activity = context;

        this.layout = layout;
        this.layoutSubViews = layoutSubViews;
    }

    public void ShowDialog(boolean cancelable) {
        try {
            String dialogText = (String) activity.getText(layoutSubViews[2]);//R.string.main_loading_label);

            this.show();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View loadingView = inflater.inflate(layout, //R.layout.loading_dialog_layout,
                    null, false);

            if (loadingView != null) {
                ImageView imageView = (ImageView) loadingView.findViewById(layoutSubViews[0]);//R.id.loading_imageView);
                TextView textView = (TextView) loadingView.findViewById(layoutSubViews[1]);//R.id.loading_textView);

                textView.setText(dialogText);

                imageView.setBackgroundResource(layoutSubViews[3]);//R.drawable.loading);

                this.setContentView(loadingView);

                AnimationDrawable anim = (AnimationDrawable) imageView.getBackground();

                if (anim != null)
                    anim.start();

                this.setCancelable(cancelable);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
