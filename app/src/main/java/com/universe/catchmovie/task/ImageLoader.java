package com.universe.catchmovie.task;

/**
 * Created by juancarloslopez on 11/13/15.
 */


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.universe.catchmovie.interfaces.IImageLoader;
import com.universe.catchmovie.utils.FileCache;
import com.universe.catchmovie.utils.MemoryCache;
import com.universe.catchmovie.utils.Utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
    //private final int REQUIRED_SIZE = 70;
    private static final int TIMEOUT = 30000;
    public IImageLoader delegate;
    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    ExecutorService executorService;
    boolean cleanCache;
    private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    private ProgressBar progressBar;
    private String logClassTag = "ImageLoaderClassException";

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        executorService = Executors.newFixedThreadPool(5);
        cleanCache = false;
    }

    //final int stub_id = R.drawable.transp;

    public void DisplayImage(String url, ImageView imageView) {
        DisplayImage(url, imageView, null);
    }

    public void DisplayImage(String url, ImageView imageView, ProgressBar _progressBar) {
        progressBar = _progressBar;
        DisplayImage(url, imageView, false);
    }

    public void DisplayImage(String url, ImageView imageView, boolean clearCache) {
        imageViews.put(imageView, url);

        if (!clearCache) {
            Bitmap bitmap = memoryCache.get(url); // GET FROM CACHE

            if (bitmap != null)
                imageView.setImageBitmap(bitmap); // LOAD CACHED IMAGE
            else {
                queuePhoto(url, imageView);
                //imageView.setImageResource(stub_id);
            }
        } else {
            this.cleanCache = true;//clearCache;
            queuePhoto(url, imageView);
            //imageView.setImageResource(stub_id);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(url, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    public Bitmap getBitmap(String url) {
        File f = fileCache.getFile(url);

        if (f != null) {
            //from SD cache
            Bitmap b = decodeFile(f);

            if (!cleanCache)
                if (b != null)
                    return b; // RETURN CACHE FILE

            URL imageUrl;
            HttpURLConnection conn = null;
            InputStream is = null;
            OutputStream os = null;
            Bitmap bitmap = null;

            //from web
            try {
                if (url.startsWith("http") || url.startsWith("www")) {
                    url = url.replaceAll(" ", "&20");

                    bitmap = null;
                    imageUrl = new URL(url);

                    conn = (HttpURLConnection) imageUrl.openConnection();

                    conn.setConnectTimeout(TIMEOUT);
                    conn.setReadTimeout(TIMEOUT);
                    conn.setInstanceFollowRedirects(true);

                    is = conn.getInputStream();
                    os = new FileOutputStream(f);

                    Utils.CopyStream(is, os);

                    bitmap = decodeFile(f);
                }
            } catch (FileNotFoundException fnfe) {
                Log.w("FILE NOT FOUND", url);
                //fnfe.printStackTrace();
            } catch (Exception ex) {
                Log.w(logClassTag, "method:getBitmap|" + (ex.getMessage() != null ? ex.getMessage() + "| url:" + url : "NULL Pointer Exception!| url:" + url));
                ex.printStackTrace();
            } finally {
                try {
                    if (conn != null)
                        conn.disconnect();
                    if (os != null)
                        os.close();
                    if (is != null)
                        is.close();
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }

            if (progressBar != null)
                progressBar.setVisibility(ProgressBar.GONE);

            return bitmap;
        }

        return null;
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            if (f != null) {
                if (f.exists()) {
                    //decode image size
                    /*BitmapFactory.Options o = new BitmapFactory.Options();
		            o.inJustDecodeBounds = true;
		            BitmapFactory.decodeStream(new FileInputStream(f), null, o);*/

                    //Find the correct scale value. It should be the power of 2.
                    //int width_tmp = o.outWidth, height_tmp = o.outHeight;
                    //int scale = 1;

		            /*while(true){

		                if((width_tmp / 2) < REQUIRED_SIZE || (height_tmp / 2) < REQUIRED_SIZE)
		                    break;

		                width_tmp /= 2;
		                height_tmp /= 2;
		                scale *= 2;
		            }*/

                    //decode with inSampleSize
                    BitmapFactory.Options o2 = new BitmapFactory.Options();
                    //o2.inSampleSize = scale;

                    return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    boolean imageViewReused(PhotoToLoad photoToLoad) {
        String tag = imageViews.get(photoToLoad.imageView);

        return (tag == null || !tag.equals(photoToLoad.url));

        /*if(tag == null || !tag.equals(photoToLoad.url))
            return true;

        return false;*/
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }

    //Task for the queue
    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            if (imageViewReused(photoToLoad))
                return;

            if (cleanCache)
                clearCache();

            Bitmap bmp = getBitmap(photoToLoad.url);
            memoryCache.put(photoToLoad.url, bmp); //SAVE ON CACHE

            if (imageViewReused(photoToLoad))
                return;

            BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
            Activity a = (Activity) photoToLoad.imageView.getContext();

            if (a != null)
                a.runOnUiThread(bd);
        }
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (delegate != null) {
                if (bitmap != null)
                    delegate.onImageLoaded(true);
                else
                    delegate.onImageLoaded(false);
            }

            if (imageViewReused(photoToLoad))
                return;

            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap);

        }
    }
}
