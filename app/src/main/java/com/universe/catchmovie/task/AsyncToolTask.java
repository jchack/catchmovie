package com.universe.catchmovie.task;

/**
 * Created by juancarloslopez on 11/13/15.
 */


import android.app.Activity;
import android.os.AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import com.universe.catchmovie.dialog.CustomLoadingDialog;
import com.universe.catchmovie.interfaces.IAsyncToolTask;

public class AsyncToolTask extends AsyncTask<Object, String, Object> {

    public IAsyncToolTask delegate = null;
    int layoutCustomLoadingDialog;
    int[] layoutCustomLoadingDialogSubViews;
    private CustomLoadingDialog dialog;
    private Activity activity;
    private boolean showPopup = true;

    /**
     * @param layoutCustomLoadingDialog         loading_dialog_layout
     * @param layoutCustomLoadingDialogSubViews id.loading_imageView, id.loading_textView
     *                                          <p/>
     *                                          string.main_loading_label
     *                                          <p/>
     *                                          drawable.loading
     */
    public AsyncToolTask(Activity context, int layoutCustomLoadingDialog, int[] layoutCustomLoadingDialogSubViews) {
        activity = context;

        this.layoutCustomLoadingDialog = layoutCustomLoadingDialog;
        this.layoutCustomLoadingDialogSubViews = layoutCustomLoadingDialogSubViews;
    }

    /**
     * @param layoutCustomLoadingDialog         loading_dialog_layout
     * @param layoutCustomLoadingDialogSubViews id.loading_imageView, id.loading_textView
     *                                          <p/>
     *                                          string.main_loading_label
     *                                          <p/>
     *                                          drawable.loading
     */
    public AsyncToolTask(Activity context, boolean _showPopup, int layoutCustomLoadingDialog, int[] layoutCustomLoadingDialogSubViews) {
        activity = context;
        showPopup = _showPopup;

        this.layoutCustomLoadingDialog = layoutCustomLoadingDialog;
        this.layoutCustomLoadingDialogSubViews = layoutCustomLoadingDialogSubViews;
    }

    @Override
    protected void onPreExecute() {
        if (showPopup) {
            dialog = new CustomLoadingDialog(activity, layoutCustomLoadingDialog, layoutCustomLoadingDialogSubViews);

            dialog.ShowDialog(false);
        }

        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object... input) {
        Object jsonObject = null;

        try {
            jsonObject = delegate.processToRun(input);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    protected void onPostExecute(Object result) {
        if (showPopup && dialog.isShowing())
            dialog.dismiss();

        delegate.processFinish(result);

        super.onPostExecute(result);
    }
}
