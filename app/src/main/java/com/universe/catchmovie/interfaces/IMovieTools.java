package com.universe.catchmovie.interfaces;

/**
 * Created by juancarloslopez on 11/13/15.
 */
import com.universe.catchmovie.objects.City;
import android.widget.ArrayAdapter;
import com.universe.catchmovie.adapters.MoviesAdapter;
import com.universe.catchmovie.objects.MoviePremiere;

public interface IMovieTools {
    public void onMovieToolsProcessFinish();

    public void setTheaterCitiesResults(ArrayAdapter<City> adapter);

    public void setMoviesAdapter(MoviesAdapter adapter);

    public void setMoviePremiere(MoviePremiere moviePremiere);
}
