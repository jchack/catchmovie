package com.universe.catchmovie.interfaces;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public interface IAsyncToolTask {
    void processFinish(Object output);

    Object processToRun(Object... input);
}