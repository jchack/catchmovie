package com.universe.catchmovie.interfaces;

import android.net.Uri;

/**
 * Created by juancarloslopez on 11/16/15.
 */
public interface OnFragmentInteractionListener {

        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);

}
