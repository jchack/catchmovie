package com.universe.catchmovie.interfaces;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public interface IImageLoader {
    public void onImageLoaded(boolean wasSuccessful);
}
