package com.universe.catchmovie.interfaces;

/**
 * Created by juancarloslopez on 11/13/15.
 */
import android.app.Activity;
import com.universe.catchmovie.dialog.CustomAlertDialog;

public interface ICustomAlertDialog {
    void onOkAlertClick(CustomAlertDialog dialog, Activity callingActivity);

    void onCancelAlertClick(CustomAlertDialog dialog, Activity callingActivity);
}
