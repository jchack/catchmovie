package com.universe.catchmovie.adapters;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Movie;
import java.util.ArrayList;

public class TheaterSchedulesAdapter extends ArrayAdapter<Movie> {

    private static LayoutInflater inflater = null;
    private ArrayList<Movie> data;

    public TheaterSchedulesAdapter(Activity activity, int textViewResourceId, ArrayList<Movie> d) {
        super(activity.getBaseContext(), textViewResourceId, d);
        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return (data != null ? data.size() : 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        try {
            if (convertView == null) {
                holder = new ViewHolder();

                convertView = inflater.inflate(R.layout.theater_detail_layout,
                        parent, false);

                if (convertView != null) {
                    holder.moviename =  (TextView)   convertView.findViewById(R.id.itemlist_header_textView);
                    holder.moviesch =   (TextView)  convertView.findViewById(R.id.theaterdetail_moviesch_textView);
                    holder.movieArrow = (ImageView) convertView.findViewById(R.id.theaterdetail_arrow_imageView);

                    convertView.setTag(holder);
                }
            } else
                holder = (ViewHolder) convertView.getTag();

            Movie movie = data.get(position);

            if (movie.getHorario() != null) {
                holder.moviename.setText(movie.getNombre());
                holder.moviename.setTag(movie.getMovieId());
                holder.moviesch.setText(movie.getHorario());
            } else {
                if(movie == null);
                else{
                    holder.moviename.setText(movie.getNombre());
                    holder.moviesch.setText(R.string.textview_movie_schedule_label);
                    holder.moviesch.setVisibility(TextView.GONE);
                    //holder.moviesch.setTypeface(Typeface.DEFAULT_BOLD);
                    holder.movieArrow.setVisibility(ImageView.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView moviename;
        public TextView moviesch;
        public ImageView movieArrow;
    }
}

