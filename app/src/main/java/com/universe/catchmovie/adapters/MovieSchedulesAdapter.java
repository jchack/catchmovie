package com.universe.catchmovie.adapters;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Theater;

import java.util.ArrayList;

public class MovieSchedulesAdapter extends ArrayAdapter<Theater> {

    private static LayoutInflater inflater = null;
    int layout;
    int[] layoutSubViews;
    private ArrayList<Theater> data;

    public MovieSchedulesAdapter(Activity activity, int textViewResourceId, ArrayList<Theater> d, int layout, int[] layoutSubViews) {
        super(activity.getBaseContext(), textViewResourceId, d);
        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.layoutSubViews = layoutSubViews;
    }

    @Override
    public int getCount() {
        return (data != null ? data.size() : 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        try {
            if (convertView == null) {
                holder = new ViewHolder();

                convertView = inflater.inflate(R.layout.theater_detail_layout,
                        parent, false);

                if (convertView != null) {
                    holder.theatername = (TextView) convertView.findViewById(R.id.theaterdetail_name_textView);
                    holder.theatersch = (TextView) convertView.findViewById(R.id.theaterdetail_moviesch_textView);
                    holder.theaterArrow = (ImageView) convertView.findViewById(R.id.theaterdetail_arrow_imageView);
                    holder.city = (TextView) convertView.findViewById(R.id.itemlist_header_textView);

                    convertView.setTag(holder);
                }
            } else
                holder = (ViewHolder) convertView.getTag();

            Theater theater = data.get(position);

            holder.theatername.setText(theater.getNombre());
            holder.theatername.setTag(theater.getTheaterId());
            holder.theatersch.setText(theater.getHorario());

            if(theater.getCiudad() != null)
                holder.city.setText(theater.getCiudad().getName());

            holder.theaterArrow.setVisibility(ImageView.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView theatername;
        public TextView theatersch;
        public ImageView theaterArrow;
        public TextView city;
    }
}

