package com.universe.catchmovie.adapters;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.utils.ListingApearingAnimation;
import com.universe.catchmovie.task.ImageLoader;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Movie;
import java.util.ArrayList;

public class MoviesAdapter extends ArrayAdapter<Movie> {

    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;
    private Activity activity;
    private ArrayList<Movie> data;
    private Movie carteleraHeaderSet = null;
    private int[] resources;
    private boolean isFromResult;

    private int mLastPosition = -1;


    public MoviesAdapter(Activity _activity, int textViewResourceId, ArrayList<Movie> _data, int[] resources, boolean isFromResult) {
        super(_activity.getBaseContext(), textViewResourceId, _data);
        activity = _activity;
        data = _data;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
        this.resources = resources;
        this.isFromResult = isFromResult;
    }

    @Override
    public int getCount() {
        return (data != null ? data.size() : 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        try {
            if (convertView == null) {
                //convertView = inflater.inflate(R.layout.itemlist_movies_layout,
                convertView = inflater.inflate(resources[0],
                        parent, false);

                holder = new ViewHolder();

                if (convertView != null) {

                    holder.moviename = (TextView) convertView.findViewById(resources[1]);//R.id.movieitem_moviename_textView);
                    holder.movieimg = (ImageView) convertView.findViewById(resources[2]);//R.id.movieitem_imageView);

                    if(isFromResult)
                        holder.moviedesc = (TextView) convertView.findViewById(resources[3]);//R.id.movieitem_description_textView);


                    convertView.setTag(holder);
                }
            } else
                holder = (ViewHolder) convertView.getTag();

            Movie movie = data.get(position);

            String name = movie.getNombre();

	    	/*if (name.split("-").length > 1) {
	    		if (EnvironmentObjects.currentLocale.getLanguage().equalsIgnoreCase("es"))
	    			name = name.split("-")[0];
	    		else
	    			name = name.split("-")[1];
	    	}*/

            holder.moviename.setText(name);
            holder.moviename.setTag(movie.getMovieId());

            holder.movieimg.setImageDrawable(null);


            if (convertView != null && isFromResult)
                if (movie.isPremiere()) {
                    LinearLayout header = (LinearLayout) convertView.findViewById(R.id.movieitem_header_layout);

                    if (position == 0) {
                        header.setVisibility(LinearLayout.VISIBLE);
                        ((TextView) header.findViewById(R.id.itemlist_header_textView)).setText(
                                activity.getText(R.string.textview_movie_header_new));
                    } else
                        header.setVisibility(LinearLayout.GONE);
                } else {
                    LinearLayout header = (LinearLayout) convertView.findViewById(R.id.movieitem_header_layout);

                    if (carteleraHeaderSet == null || movie == carteleraHeaderSet) {
                        header.setVisibility(LinearLayout.VISIBLE);
                        ((TextView) header.findViewById(R.id.itemlist_header_textView)).setText(
                                activity.getText(R.string.textview_movie_header_nowshowing));
                        carteleraHeaderSet = movie;
                    } else
                        header.setVisibility(LinearLayout.GONE);
                }

            imageLoader.DisplayImage("http://ads.amarillas.com.do/cine/movie" + String.valueOf(movie.getMovieId()) + ".jpg", holder.movieimg);


            if(isFromResult)
                holder.moviedesc.setText(
                        movie.getGenero()!=null?
                                (!movie.getGenero().equalsIgnoreCase("null")?movie.getGenero():""):"");

            if (position > mLastPosition && position > 14) {
                new ListingApearingAnimation().Animate(convertView);

                mLastPosition = position;
            }
        } catch (Exception e) {
            convertView = inflater.inflate(resources[0],
                    parent, false);

            if (convertView != null)
                convertView.setVisibility(View.GONE);
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView moviename;
        public ImageView movieimg;
        public TextView moviedesc;
    }


}

