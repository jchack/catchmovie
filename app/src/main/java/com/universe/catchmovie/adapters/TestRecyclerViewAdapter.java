package com.universe.catchmovie.adapters;

/**
 * Created by apple on 11/26/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;

import com.universe.catchmovie.MovieDetailsActivity;
import com.universe.catchmovie.R;
import com.universe.catchmovie.Resources.ResourcesReference;
import com.universe.catchmovie.objects.Movie;
import com.universe.catchmovie.objects.MoviePremiere;

import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class TestRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener{

    List<Object> contents;

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;
    private Activity activity;
    private MoviePremiere mMoviePremier;

    public TestRecyclerViewAdapter(List<Object> contents, Activity activity, MoviePremiere mMoviePremier) {
        this.contents = contents;
        this.activity = activity;
        this.mMoviePremier = mMoviePremier;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_home_movie_slider, parent, false);

        MoviesAdapter mMovieAdapterThreater = new MoviesAdapter(
                activity,0,mMoviePremier.getCarteleraArray(),ResourcesReference.MovieResourceCover,false);

        MoviesAdapter mMovieAdapterRelease = new
                MoviesAdapter(activity,0,mMoviePremier.getEstrenosArray(),ResourcesReference.MovieResourceCover,false);


        TableRow mTableRowThreater = (TableRow) view.findViewById(R.id.movies_slider_tableRowThreater);
        TableRow mTableRowRelease = (TableRow) view.findViewById(R.id.movies_slider_tableRowRelease);

        for (int i = 0; i < mMovieAdapterThreater.getCount();i++){
            View v = mMovieAdapterThreater.getView(i, null, mTableRowThreater);
            v.setOnClickListener(this);
            mTableRowThreater.addView(v);
        }

        for (int i = 0; i < mMovieAdapterRelease.getCount();i++){
            View v = mMovieAdapterRelease.getView(i, null, mTableRowRelease);
            v.setOnClickListener(this);
            mTableRowRelease.addView(v);
        }

        return new RecyclerView.ViewHolder(view) {};

//        switch (viewType) {
//            case TYPE_HEADER: {
//                view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.fragment_home_movie_slider, parent, false);
//                return new RecyclerView.ViewHolder(view) {
//                };
//            }
//            case TYPE_CELL: {
//                view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.list_item_card_small, parent, false);
//                return new RecyclerView.ViewHolder(view) {
//                };
//            }
//        }
       // return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                break;
            case TYPE_CELL:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if(v != null) {
            Intent i = new Intent(activity, MovieDetailsActivity.class);
            i.putExtra(activity.getString(R.string.movieIDParam),
                    String.valueOf(((MoviesAdapter.ViewHolder) v.getTag()).moviename.getTag()));

            activity.startActivity(i);
        }
    }
}