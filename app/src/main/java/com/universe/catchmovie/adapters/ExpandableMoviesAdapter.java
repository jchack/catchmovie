package com.universe.catchmovie.adapters;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Movie;
import com.universe.catchmovie.objects.MovieByGenre;

import java.util.ArrayList;

public class ExpandableMoviesAdapter extends BaseExpandableListAdapter implements OnClickListener {

    public ArrayList<MovieByGenre> groupItem;
    public LayoutInflater minflater;
    public Activity activity;
    public Fragment fragmentActivity;
    private Class<?> movieDetailsActivity;

    public ExpandableMoviesAdapter(ArrayList<MovieByGenre> grList, Class<?> _movieDetailsActivity) {
        groupItem = grList;
        movieDetailsActivity = _movieDetailsActivity;
    }

    public void setInflater(LayoutInflater mInflater, Activity act) {
        this.minflater = mInflater;
        activity = act;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MovieByGenre tempChild = groupItem.get(groupPosition);

        Movie currentMovie = tempChild.getPeliculas().get(childPosition);

        TextView movieName;
        ImageView movieImage;

        if (convertView == null)
            convertView = minflater.inflate(
                    R.layout.itemlist_movies_layout,
                    parent, false);

        if (convertView != null) {
            LinearLayout layout = (LinearLayout) convertView.findViewById(R.id.movieitem_moviename_linearLayout);

            movieName = (TextView) convertView.findViewById(R.id.movieitem_moviename_textView);
            movieName.setText(currentMovie.getNombre());
            movieName.setTag(currentMovie.getMovieId());
            movieName.setGravity(Gravity.CENTER_VERTICAL);

            if(layout != null)
                layout.setPadding(40, 0, 0, 0);

            ((TextView)convertView.findViewById(R.id.movieitem_description_textView)).setVisibility(View.GONE);

            movieImage = (ImageView) convertView.findViewById(R.id.movieitem_imageView);
            movieImage.setVisibility(ImageView.GONE);

            convertView.setTag(currentMovie);

            convertView.setOnClickListener(this);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (groupItem.get(groupPosition).getPeliculas()).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return groupItem.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = minflater.inflate(
                    R.layout.itemlist_checked_textview_layout,
                    parent, false);

        if (convertView != null) {
            ((CheckedTextView) convertView).setText(groupItem.get(groupPosition).getGenero());
            ((CheckedTextView) convertView).setChecked(isExpanded);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onClick(View view) {
        if (fragmentActivity != null) {
            Movie movie = (Movie) view.getTag();

            Intent intent = new Intent(activity, movieDetailsActivity);

            intent.putExtra(activity.getResources().getString(R.string.movieIDParam), String.valueOf(movie.getMovieId()));

            activity.startActivity(intent);
        }
    }
}
