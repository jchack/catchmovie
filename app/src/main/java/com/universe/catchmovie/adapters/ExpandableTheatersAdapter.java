package com.universe.catchmovie.adapters;

/**
 * Created by juancarloslopez on 11/13/15.
 */
import com.universe.catchmovie.task.AsyncToolTask;
import com.universe.catchmovie.interfaces.IAsyncToolTask;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Theater;
import com.universe.catchmovie.Resources.ResourcesReference;

import java.util.ArrayList;

public class ExpandableTheatersAdapter extends BaseExpandableListAdapter implements OnClickListener {

    public ArrayList<Theater> groupItem, tempChild;
    public ArrayList<ArrayList<Theater>> ChildItem = new ArrayList<ArrayList<Theater>>();
    public LayoutInflater minflater;
    public Activity activity;
    public Fragment fragmentActivity;

    public ExpandableTheatersAdapter(ArrayList<Theater> grList, ArrayList<ArrayList<Theater>> childItem) {
        groupItem = grList;
        this.ChildItem = childItem;
    }

    public void setInflater(LayoutInflater mInflater, Activity act) {
        this.minflater = mInflater;
        activity = act;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        tempChild = ChildItem.get(groupPosition);

        Theater currentTheater = tempChild.get(childPosition);

        TextView theaterName;
        TextView theaterAddress;

        if (convertView == null)
            convertView = minflater.inflate(
                    R.layout.itemlist_theather_layout,
                    parent, false);

        if (convertView != null) {
            theaterName = (TextView) convertView.findViewById(R.id.itemlist_theatername_textView);
            theaterName.setText(currentTheater.getNombre());
            theaterName.setTag(currentTheater.getTheaterId());

            theaterAddress = (TextView) convertView.findViewById(R.id.itemlist_theateraddress_textView);
            theaterAddress.setText(currentTheater.getDireccion());
            theaterAddress.setTag(currentTheater.getCiudad());
            theaterAddress.setVisibility(View.VISIBLE);

            convertView.setTag(currentTheater);

            convertView.setOnClickListener(this);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return (ChildItem.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return groupItem.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = minflater.inflate(
                    R.layout.itemlist_checked_textview_layout,
                    parent, false);

        if (convertView != null) {
            ((CheckedTextView) convertView).setText(groupItem.get(groupPosition).getNombre());
            ((CheckedTextView) convertView).setChecked(isExpanded);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void onClick(View view) {
        if (fragmentActivity != null) {
            Theater theater = (Theater) view.getTag();

            AsyncToolTask tool = new AsyncToolTask(activity, R.layout.loading_dialog_layout, ResourcesReference.CustomLoadingDialogSubViews);
            tool.delegate = (IAsyncToolTask) fragmentActivity;
            tool.execute(theater.getCiudad().getCode(), theater.getTheaterId());
        }
    }
}

