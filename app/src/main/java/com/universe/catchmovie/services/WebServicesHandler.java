package com.universe.catchmovie.services;

import android.content.Context;
import android.util.Log;
import com.universe.catchmovie.constants.Constants;
import com.universe.catchmovie.enums.Languages;
import com.universe.catchmovie.enums.HTTP_Request_Method;
import com.universe.catchmovie.R;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;
import java.io.IOException;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public class WebServicesHandler {

    private HttpClient client = new HttpClient();
    private ResponseStatus responseStatus;


    /**
     * MOVIES*
     */
    public String GetEstrenos(Context activity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(activity.getString(R.string.request_language_code), Languages.ES.toString());
            jsonObject.put(activity.getString(R.string.request_operation), Constants.BUSINESSUNIT);
            jsonObject.put(activity.getString(R.string.request_actual_page),"0");
            jsonObject.put(activity.getString(R.string.request_page_size),"0");
            jsonObject.put(activity.getString(R.string.request_device_id), Constants.DEVICE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return DoRequest("GetEstrenos", jsonObject, Constants.MOVIE_PREMIERE, false, activity, HTTP_Request_Method.GET);
    }

    public String GetMoviesCities(Context activity, String city_code)  {
        JSONObject json = new JSONObject();
        try{
            json.put(activity.getString(R.string.request_city_code),city_code);
            json.put(activity.getString(R.string.request_device_id), Constants.DEVICE);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return DoRequest("GetMovieCities", json, Constants.THEATER_GET_CITIES, false, activity,HTTP_Request_Method.GET);
    }

    public String SaveMovieVote(String movieID, String rating, String userid, Context activity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(activity.getString(R.string.request_user_id), (userid != null ? userid : ""));
            jsonObject.put(activity.getString(R.string.request_movie_id), movieID);
            jsonObject.put(activity.getString(R.string.request_movie_vote), rating);
            jsonObject.put(activity.getString(R.string.request_device_id), Constants.DEVICE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return DoRequest("SaveMovieVote", jsonObject, Constants.MOVIE_SAVE_VOTE, false, activity,HTTP_Request_Method.PUT);
    }

    public String GetTheaterByCity(String city, Context activity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(activity.getString(R.string.request_city_code), city);
            jsonObject.put(activity.getString(R.string.request_device_id), Constants.DEVICE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return DoRequest("GetTheaterByCity", jsonObject, Constants.THEATER_BY_CITY, false, activity,HTTP_Request_Method.GET);
    }

    public String GetMovieData(String movieID, Context activity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(activity.getString(R.string.request_movie_id), movieID);
            jsonObject.put(activity.getString(R.string.request_language_code),Languages.ES);
            jsonObject.put(activity.getString(R.string.request_device_id),Constants.DEVICE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return DoRequest("GetMovieData", jsonObject, Constants.MOVIE_GET_DATA, false, activity,HTTP_Request_Method.GET);
    }

    public String GetMovieSchedules(String city, String theaterID, Context activity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(activity.getString(R.string.request_city_code),  city);
            jsonObject.put(activity.getString(R.string.request_theater_id),theaterID);
            jsonObject.put(activity.getString(R.string.request_device_id), Constants.DEVICE);
            jsonObject.put(activity.getString(R.string.request_language_code), Languages.ES);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return DoRequest("GetMovieSchedules", jsonObject, Constants.MOVIE_SCHEDULES, false, activity, HTTP_Request_Method.GET);
    }

    public String GetMoviesByGenre(Context activity) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(activity.getString(R.string.request_language_code), Languages.ES.toString());
            jsonObject.put(activity.getString(R.string.request_device_id),Constants.DEVICE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return DoRequest("GetMoviesByGenre", jsonObject, Constants.MOVIE_BY_GENRE, false, activity,HTTP_Request_Method.GET);
    }


    private String DoRequest(
            String logClassName,
            JSONObject jsonObject,
            String serviceURL,
            boolean isSearch,
            Context activity
            ,HTTP_Request_Method HTTP_Method) {
        responseStatus = new ResponseStatus();
        responseStatus.setDescription("Success");
        responseStatus.setServiceName(logClassName);

        String httpJsonRequest = "";

        try {
            Log.d("REQUEST URL", serviceURL);

            jsonObject.put(activity.getString(R.string.request_info_site_id), Constants.SITEID);
            jsonObject.put(activity.getString(R.string.request_operation), Constants.BUSINESSUNIT);

            httpJsonRequest = client.httpURLJsonRequest(serviceURL, jsonObject, HTTP_Method, activity.getString(R.string.SERVICE_AUTHENTICATION));

            //if (isSearch) {
            if (httpJsonRequest == null) {
                responseStatus = new ResponseStatus();
                responseStatus.setDescription("No records");
                responseStatus.setServiceName(logClassName);

                Log.e("RESPONSE STATUS", "RESPONSE WAS NULL!");

                return null;
            } else if (!(new JSONObject(httpJsonRequest).has(activity.getResources().getString(R.string.response_BASE)))) {
                responseStatus = new ResponseStatus();
                responseStatus.setDescription("No records");
                responseStatus.setServiceName(logClassName);

                Log.e("RESPONSE STATUS", "RESPONSE DIDN'T HAVE 'd'!");

                return null;
            } else if (httpJsonRequest.trim().length() == 0)
                Log.e("RESPONSE STATUS", "RESPONSE WAS EMPTY!");
            else {
                Log.d("RESPONSE STATUS", "SUCCESS!!! LENGHT: " + httpJsonRequest.trim().length());
                Log.d("JSON RESPONSE", httpJsonRequest);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
               } catch (IOException e) {
            e.printStackTrace();
              } catch (Exception e) {
            e.printStackTrace();
        }

        return httpJsonRequest;
    }

}
