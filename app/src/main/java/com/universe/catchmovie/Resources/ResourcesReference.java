package com.universe.catchmovie.Resources;

import com.universe.catchmovie.R;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public class ResourcesReference {

    //use
    public static int[] CustomAlertDialogSubViews = {
            R.id.alert_header_textView,
            R.id.alert_body_textView,
            R.id.alert_ok_button,
            R.id.alert_cancel_button,
            R.id.alert_customview_layout};

    public static int[] CustomLoadingDialogSubViews = {
            R.id.loading_imageView,
            R.id.loading_textView,
            R.string.main_loading_label,

            R.drawable.loading
    };

    public static int[] MovieSchedulesAdapterSubViews = {
           /* R.id.theaterdetail_moviename_textView*/
            R.id.itemlist_header_textView,
            R.id.theaterdetail_moviesch_textView,
            R.id.theaterdetail_arrow_imageView,
            R.id.theaterdetail_name_textView
    };

    public static int[] MoviesAdapterSubViews = {
            R.id.movieitem_moviename_textView,
            R.id.movieitem_imageView,
            R.id.movieitem_header_layout,
            R.id.itemlist_header_textView,

            R.string.textview_movie_header_new,
            R.string.textview_movie_header_nowshowing
    };

    public static int[] TheaterSchedulesAdapterSubViews = {
           /* R.id.theaterdetail_moviename_textView*/
            R.id.itemlist_header_textView,
            R.id.theaterdetail_moviesch_textView,
            R.id.theaterdetail_arrow_imageView,

            R.string.textview_movie_header_label,
            R.string.textview_movie_schedule_label
    };


    public static int[] MovieResourceCover = {
       R.layout.movie_cover_layout,
       R.id.movie_cover_text,
       R.id.movie_cover_image
    };

    public static int[] MovieResource = {
            R.layout.itemlist_movies_layout,
            R.id.movieitem_moviename_textView,
            R.id.movieitem_imageView,
            R.id.movieitem_description_textView
    };

}
