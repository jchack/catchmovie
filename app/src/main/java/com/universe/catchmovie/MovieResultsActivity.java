package com.universe.catchmovie;

/**
 * Created by juancarloslopez on 11/13/15.
 */


import com.universe.catchmovie.Resources.ResourcesReference;
import com.universe.catchmovie.enums.Languages;
import com.universe.catchmovie.objects.City;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.universe.catchmovie.task.AsyncToolTask;
import com.universe.catchmovie.interfaces.IAsyncToolTask;
import com.universe.catchmovie.adapters.ExpandableTheatersAdapter;
import com.universe.catchmovie.adapters.MoviesAdapter;
import com.universe.catchmovie.objects.MovieByGenre;
import com.universe.catchmovie.fragments.MoviesFragmentActivity;
import com.universe.catchmovie.fragments.TheaterCityFragmentActivity;
import com.universe.catchmovie.objects.MoviePremiere;
import com.universe.catchmovie.interfaces.IMovieTools;
import com.universe.catchmovie.utils.MovieTools;


import java.util.ArrayList;

public class MovieResultsActivity extends BaseActivity implements OnClickListener, IMovieTools, IAsyncToolTask {

    private static final int MOVIES_STATE = 0x1;
    private static final int THEATERS_STATE = 0x2;
    private static final int GENDER_STATE = 0x3;
    public MoviePremiere moviePremiere;
    public MoviesAdapter moviesAdapter;
    public ExpandableTheatersAdapter theatersAdapter;
    public ArrayAdapter<City> theaterCitiesResult;
    //public TheaterSchedulesAdapter theaterSchedulesAdapter;
    public MoviesAdapter theaterSchedulesAdapter;
    public ArrayList<MovieByGenre> movieByGender;
    public ToggleButton firstSwitchButton;
    public ToggleButton secondSwitchButton;
    public ToggleButton thirdSwitchButton;
    public TextView header;
    private int mTabState;
    private City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_results);
        setupActionBar();
//        setContentView(R.layout.activity_main);


        initComponent();

        header.setText(R.string.textview_movie_header_theaters);
        header.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        header.setGravity(Gravity.CENTER_HORIZONTAL);

        MovieTools movieTools = new MovieTools(Languages.ES.toString(), webServicesHandler, this);

        movieTools.delegate = this;

        city = new City("SD", "Santo Domingo", false, 0);

        movieTools.doConsult(MovieTools.consultType.GET_ALL_DATA, this, city, null, null);


    }

    private void initComponent(){

        firstSwitchButton = (ToggleButton) findViewById(R.id.itemlist_switch_movies_toggleButton);
        secondSwitchButton = (ToggleButton) findViewById(R.id.itemlist_switch_gender_toggleButton);
        thirdSwitchButton  = (ToggleButton) findViewById(R.id.itemlist_switch_theaters_toggleButton);
        header = (TextView) findViewById(R.id.itemlist_header_textView);

        firstSwitchButton.setOnClickListener(this);
        secondSwitchButton.setOnClickListener(this);
        thirdSwitchButton.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMovieToolsProcessFinish() {
        gotoMovieView(false);
        firstSwitchButton.setChecked(true);
    }

    @Override
    public void setTheaterCitiesResults(ArrayAdapter<City> adapter) {
        theaterCitiesResult = adapter;
    }

    @Override
    public void setMoviesAdapter(MoviesAdapter adapter) {
        moviesAdapter = adapter;
    }

    @Override
    public void setMoviePremiere(MoviePremiere moviePremiere) {
        this.moviePremiere = moviePremiere;
    }

    public void gotoMovieView(boolean isByGender) {
        findViewById(R.id.itemlist_headerlabel_include).setVisibility(LinearLayout.GONE);

        Bundle arg = new Bundle();
        arg.putBoolean("genre_filter", isByGender);

        // mTabState keeps track of which tab is currently displaying its contents.
        // Perform a check to make sure the list tab content isn't already displaying.

        if (mTabState != MOVIES_STATE || !isByGender) {
            // Update the mTabState
            mTabState = MOVIES_STATE;

            // Fragments have access to their parent Activity's FragmentManager. You can
            // obtain the FragmentManager like this.


            FragmentManager fm = getSupportFragmentManager();

            if (fm != null) {
                // Perform the FragmentTransaction to load in the list tab content.
                // Using FragmentTransaction#replace will destroy any Fragments
                // currently inside R.id.fragment_content and add the new Fragment
                // in its place.

                MoviesFragmentActivity fragment =  new MoviesFragmentActivity();

                fragment.setArguments(arg);

                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.itemlist_layout,fragment );
                ft.commit();
            }
        }
    }

    public void gotoTheaterView() {
        findViewById(R.id.itemlist_headerlabel_include).setVisibility(LinearLayout.GONE);

        // mTabState keeps track of which tab is currently displaying its contents.
        // Perform a check to make sure the list tab content isn't already displaying.

        if (mTabState != THEATERS_STATE) {
            // Update the mTabState
            mTabState = THEATERS_STATE;

            // Fragments have access to their parent Activity's FragmentManager. You can
            // obtain the FragmentManager like this.

            FragmentManager fm = getSupportFragmentManager();

            if (fm != null) {
                // Perform the FragmentTransaction to load in the list tab content.
                // Using FragmentTransaction#replace will destroy any Fragments
                // currently inside R.id.fragment_content and add the new Fragment
                // in its place.
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.itemlist_layout, new TheaterCityFragmentActivity());
                ft.commit();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.itemlist_switch_movies_toggleButton) {
            // Switch the tab content to display the movies view.
            firstSwitchButton.setChecked(true);
            secondSwitchButton.setChecked(false);
            thirdSwitchButton.setChecked(false);

            changeColorToggle(firstSwitchButton, secondSwitchButton, thirdSwitchButton);

            gotoMovieView(false);

        } else if (view.getId() == R.id.itemlist_switch_gender_toggleButton) {
            // Switch the tab content to display the theaters view.
            firstSwitchButton.setChecked(false);
            secondSwitchButton.setChecked(true);
            thirdSwitchButton.setChecked(false);

            changeColorToggle(secondSwitchButton,firstSwitchButton,thirdSwitchButton);

            AsyncToolTask task =
                    new AsyncToolTask(this, R.layout.loading_dialog_layout, ResourcesReference.CustomLoadingDialogSubViews);

            task.delegate = this;
            task.execute();

        }else if (view.getId() == R.id.itemlist_switch_theaters_toggleButton) {
            // Switch the tab content to display the theaters view.
            firstSwitchButton.setChecked(false);
            secondSwitchButton.setChecked(false);
            thirdSwitchButton.setChecked(true);

            changeColorToggle(secondSwitchButton,firstSwitchButton,thirdSwitchButton);

            gotoTheaterView();
        }
    }

    @Override
    public void processFinish(Object output) {
        if (output != null && output instanceof ArrayList<?>) {

            movieByGender = (ArrayList<MovieByGenre>) output;
            mTabState = GENDER_STATE;

            gotoMovieView(true);
        }
    }

    @Override
    public Object processToRun(Object... input) {
        String output;
        ArrayList<MovieByGenre> moviesByGenreCollection = null;
        try{
            output = webServicesHandler.GetMoviesByGenre(this);

            if(output != null){
                MovieTools movieTool =
                        new MovieTools(Languages.ES.toString(),
                                webServicesHandler, this);
                moviesByGenreCollection = movieTool.parseMovieByGenre(output, this);

            }
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return moviesByGenreCollection;
    }

    public void changeToMovieByGender(){

        firstSwitchButton.setChecked(false);
        secondSwitchButton.setChecked(true);
        thirdSwitchButton.setChecked(false);

        changeColorToggle(secondSwitchButton,firstSwitchButton,thirdSwitchButton);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}

