package com.universe.catchmovie;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ToggleButton;


import com.universe.catchmovie.constants.Constants;
import com.universe.catchmovie.enums.Config;
import com.universe.catchmovie.interfaces.OnFragmentInteractionListener;
import com.universe.catchmovie.objects.MoviePremiere;
import com.universe.catchmovie.services.WebServicesHandler;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener {

    /**WEBSERVICE HANDLER INSTANCE**/
    public static WebServicesHandler webServicesHandler = new WebServicesHandler();
    public MoviePremiere mMoviePremier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Constants.initialize(Config.QA,true);

    }

    public void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.show();
        }
    }


    /***************CHANGE COLOR TOGGLE****************/
    public void changeColorToggle(ToggleButton a, ToggleButton b, ToggleButton c){

        if(a != null && a.isChecked()){
            a.setTextColor(getResources().getColor(R.color.base_gray));
            if(b != null)
                b.setTextColor(getResources().getColor(R.color.stroke_color_gray));
            if(c != null)
                c.setTextColor(getResources().getColor(R.color.stroke_color_gray));
        }else if(b != null && b.isChecked()){

            if(a != null)
                a.setTextColor(getResources().getColor(R.color.stroke_color_gray));
            ////////////
            b.setTextColor(getResources().getColor(R.color.base_gray));
            if(c != null)
                c.setTextColor(getResources().getColor(R.color.stroke_color_gray));

        }else if(c != null && c.isChecked()){

            if(a != null)
                a.setTextColor(getResources().getColor(R.color.stroke_color_gray));
            ////////////
            if(b != null)
                b.setTextColor(getResources().getColor(R.color.stroke_color_gray));

            c.setTextColor(getResources().getColor(R.color.base_gray));

        }

    }
    /***************TOGGLE BUTTON****************/
    public void changeToggleState(ToggleButton a, ToggleButton b, ToggleButton c){

        if(a != null && a.isChecked()){

            if(b != null)
                b.setChecked(false);
            if(c != null)
                c.setChecked(false);
        }else if(b != null && b.isChecked()){

            if(a != null)
                a.setChecked(false);
            ////////////
            if(c != null)
                c.setChecked(false);

        }else if(c != null && c.isChecked()){

            if(a != null)
                a.setChecked(false);
            ////////////
            if(b != null)
                b.setChecked(false);

        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }
}
