package com.universe.catchmovie.utils;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.interfaces.IMovieTools;
import com.universe.catchmovie.objects.City;
import com.universe.catchmovie.services.WebServicesHandler;
import com.universe.catchmovie.task.AsyncToolTask;
import com.universe.catchmovie.interfaces.IAsyncToolTask;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.json.JSONArray;
import org.json.JSONObject;
import com.universe.catchmovie.R;
import com.universe.catchmovie.adapters.MoviesAdapter;
import com.universe.catchmovie.objects.*;
import com.universe.catchmovie.Resources.ResourcesReference;

import java.util.ArrayList;

public class MovieTools implements IAsyncToolTask {

    private static consultType type;
    public IMovieTools delegate;
    private String lang;
    private WebServicesHandler webServicesHandler;
    private Context context;

    public MovieTools(String _lang, WebServicesHandler _webServicesHandler, Context context) {
        this.lang = _lang;
        this.webServicesHandler = _webServicesHandler;
        this.context = context;
    }

    public void doConsult(consultType _type, Activity _activity, City _city, Theater _theater, Movie _movie) {
        type = _type;

        String city_code = "";
        String theater = "";
        String movie = "";

        if (_city != null)
            city_code = _city.getCode();
        if (_theater != null)
            theater = String.valueOf(_theater.getTheaterId());
        if (_movie != null)
            movie = String.valueOf(_movie.getMovieId());

        AsyncToolTask asyncTool = new AsyncToolTask(_activity, R.layout.loading_dialog_layout, ResourcesReference.CustomLoadingDialogSubViews);
        asyncTool.delegate = this;
        asyncTool.execute(_type, _activity, city_code, theater, movie);
    }

    public MoviePremiere parseMoviePremiere(String json, Context context) {
        MoviePremiere premiere = null;

        try {
            JSONObject base = new JSONObject(json);
            JSONArray outer = base.getJSONArray(context.getString(R.string.response_BASE));

            premiere = new MoviePremiere();

            for (int i = 0; i < outer.length(); i++) {
                Movie currentMovie = new Movie(outer.getJSONObject(i), context);

                if (currentMovie.isPremiere())
                    premiere.getEstrenosArray().add(currentMovie);
                else
                    premiere.getCarteleraArray().add(currentMovie);
            }
        } catch (Exception e) {
            Log.v("", e.toString());
        }

        return premiere;
    }

    public ArrayList<MovieByGenre> parseMovieByGenre(String json, Context context) {
        ArrayList<MovieByGenre> movieCollection = null;

        try {
            JSONObject base = new JSONObject(json);
            JSONArray outer = base.getJSONArray(context.getString(R.string.response_BASE));

            movieCollection = new ArrayList<MovieByGenre>();

            for (int i = 0; i < outer.length(); i++)
                movieCollection.add(new MovieByGenre(outer.getJSONObject(i), context));
        } catch (Exception e) {
            Log.v("", e.toString());
        }

        return movieCollection;
    }

    public MovieDetails parseMovieDetails(String json, Context context) {
        try {
            JSONObject base = new JSONObject(json);

            return new MovieDetails(base.getJSONObject(context.getString(R.string.response_BASE)), context);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<City> parseTheaterCity(String json, Context context) {
        ArrayList<City> ciudades = null;

        try {
            JSONObject base = new JSONObject(json);
            JSONArray outer = base.getJSONArray(context.getString(R.string.response_BASE));

            ciudades = new ArrayList<City>();

            for (int i = 0; i < outer.length(); i++)
                ciudades.add(City.createNewCity(outer.getJSONObject(i), i, context));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return ciudades;
    }

    public ArrayList<Theater> parseTheaterByCity(String json, City city, Context context) {
        try {
            JSONObject base = new JSONObject(json);
            JSONArray outer = base.getJSONArray(context.getString(R.string.response_BASE));

            ArrayList<Theater> theaterGrupo = new ArrayList<Theater>();

            for (int i = 0; i < outer.length(); i++)
                theaterGrupo.add(new Theater(outer.getJSONObject(i), city, context));

            return theaterGrupo;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public ArrayList<Movie> parseMovieSchedules(String json, Context context) {
        try {
            ArrayList<Movie> movies = null;

            JSONObject base = new JSONObject(json);
            JSONArray outer = base.getJSONArray(context.getString(R.string.response_BASE));


            if(outer != null){


                movies = new ArrayList<>();

                for(int i = 0; i < outer.length(); i++){

                    movies.add(new Movie(outer.getJSONObject(i), context));
                }
            }

            return movies;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public MovieVoteResults parseVotesResult(String json, Context context) {
        MovieVoteResults rVotacion = null;

        try {
            JSONObject base = new JSONObject(json);

            rVotacion = new MovieVoteResults(base.getJSONObject(context.getString(R.string.response_BASE)), context);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return rVotacion;
    }

    @Override
    public void processFinish(Object output) {
        if (type == consultType.GET_ALL_DATA)
            delegate.onMovieToolsProcessFinish();
    }

    @Override
    public Object processToRun(Object... input) {
        Activity activity = (Activity) input[1];

        if (type == consultType.THEATER_CITIES) {
            String moviecities = webServicesHandler.GetMoviesCities(context, input[2].toString());

            ArrayList<City> result = parseTheaterCity(moviecities, context);

            ArrayAdapter<City> adapter = new ArrayAdapter<City>(activity,
                    R.layout.itemlist_categories_layout,
                    R.id.itemlist_category_textView,
                    result);

            delegate.setTheaterCitiesResults(adapter);
        } else if (type == consultType.THEATER_GET_PREMIERE) {
            String estrenos = webServicesHandler.GetEstrenos(context);

            MoviePremiere result = parseMoviePremiere(estrenos, context);

            MoviesAdapter adapter = new MoviesAdapter(activity,
                    R.id.movieitem_moviename_textView,
                    result.getEstrenosArray(),ResourcesReference.MovieResource,true);

            delegate.setMoviesAdapter(adapter);
        } else if (type == consultType.GET_ALL_DATA) {
            String estrenos = webServicesHandler.GetEstrenos(context);
            String moviecities = webServicesHandler.GetMoviesCities(context, input[2].toString());

            delegate.setMoviePremiere(parseMoviePremiere(estrenos, context));

            ArrayList<City> result2 = parseTheaterCity(moviecities, context);

            ArrayAdapter<City> adapter = new ArrayAdapter<City>(activity,
                    R.layout.itemlist_categories_layout,
                    R.id.itemlist_category_textView,
                    result2);

            delegate.setTheaterCitiesResults(adapter);
        }

        return null;
    }

    public static enum consultType {
        THEATER_CITIES,
        THEATER_GET_PREMIERE,
        GET_ALL_DATA
    }
}

