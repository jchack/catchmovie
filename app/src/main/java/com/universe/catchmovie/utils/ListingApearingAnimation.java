package com.universe.catchmovie.utils;

import android.os.Build;
import android.view.View;
import android.view.animation.AnimationUtils;

/**
 * Created by juancarloslopez on 11/13/15.
 */
import com.universe.catchmovie.R;

public class ListingApearingAnimation {

    // SET OF ANIMATIONS
    /*AnimationSet animationSet;

	// ANIMATIONS
    TranslateAnimation translateAnimation;
    AlphaAnimation alphaAnimation;
	ScaleAnimation scaleAnimation;*/

    public ListingApearingAnimation() {
        // MOVE ANIMATION
		/*translateAnimation = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, 1.0f,
            Animation.RELATIVE_TO_SELF, 0.0f);

		translateAnimation.setDuration(500);

		// FADE ANIMATION
		alphaAnimation = new AlphaAnimation(0.0f, 1.0f);

		alphaAnimation.setDuration(800);

		// SCALE ANIMATION
		scaleAnimation = new ScaleAnimation(
				1.5f, 1.0f,
				1.5f, 1.0f,
				Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);

		scaleAnimation.setDuration(500);

		// SET OF ANIMATIONS
		animationSet = new AnimationSet(false);//false mean don't share interpolators

        animationSet.addAnimation(translateAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);*/
    }

    public void Animate(View view) {
        if (view != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            //view.startAnimation(animationSet);
            view.startAnimation(AnimationUtils.loadAnimation(view.getContext(), R.anim.list_animation));
        }
    }
}

