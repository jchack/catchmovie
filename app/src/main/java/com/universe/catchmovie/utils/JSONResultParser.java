package com.universe.catchmovie.utils;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by juanmanuelreyes on 5/22/14.
 */
public class JSONResultParser {

    private JSONObject jsonObject;

    public JSONResultParser(JSONObject _jsonObject) {
        this.jsonObject = _jsonObject;
    }

    private String checkParams(String params, JSONObject _jsonObject) {
        String returnedParam = null;

        String[] paramsArray = params.split(",");

        for (String item : paramsArray)
            if (_jsonObject.has(item))
                returnedParam = item;

        return returnedParam;
    }

    public boolean hasValue(String paramName, String paramNameOld) {
        return hasValue(paramName, paramNameOld, jsonObject);
    }

    public boolean hasValue(String paramName, String paramNameOld, JSONObject _jsonObject) {
        boolean value = false;

        String param = paramName;

        param = checkParams(param, _jsonObject);

        if (param != null && _jsonObject.has(param))
            try {
                value = true;
            } catch (Exception e) {
                e.printStackTrace();
            }

        return value;
    }

    public JSONArray getJSONArrayValue(String paramName, String paramNameOld) {
        return getJSONArrayValue(paramName, paramNameOld, null, jsonObject);
    }

    public JSONArray getJSONArrayValue(String paramName, String paramNameOld, JSONObject _jsonObject) {
        return getJSONArrayValue(paramName, paramNameOld, null, _jsonObject);
    }

    public JSONArray getJSONArrayValue(String paramName, String paramNameOld, JSONArray alternateValue) {
        return getJSONArrayValue(paramName, paramNameOld, alternateValue, jsonObject);
    }

    public JSONArray getJSONArrayValue(String paramName, String paramNameOld, JSONArray alternateValue, JSONObject _jsonObject) {
        JSONArray value = alternateValue;

        String param = paramName;

        param = checkParams(param, _jsonObject);

        if (param != null && _jsonObject.has(param))
            try {
                Object object = _jsonObject.get(param);

                if (object instanceof JSONArray)
                    value = (JSONArray) object;
                //value = _jsonObject.getJSONArray(param);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return value;
    }

    public JSONObject getJSONObjectValue(String paramName, String paramNameOld) {
        return getJSONObjectValue(paramName, paramNameOld, jsonObject);
    }

    public JSONObject getJSONObjectValue(String paramName, String paramNameOld, JSONObject _jsonObject) {
        JSONObject value = null;

        String param = paramName;

        param = checkParams(param, _jsonObject);

        if (param != null && _jsonObject.has(param))
            try {
                value = _jsonObject.getJSONObject(param);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return value;
    }

    public String getStringValue(String paramName, String paramNameOld) {
        return getStringValue(paramName, paramNameOld, null, jsonObject);
    }

    public String getStringValue(String paramName, String paramNameOld, String alternateValue) {
        return getStringValue(paramName, paramNameOld, alternateValue, jsonObject);
    }

    public String getStringValue(String paramName, String paramNameOld, JSONObject _jsonObject) {
        return getStringValue(paramName, paramNameOld, null, _jsonObject);
    }

    public String getStringValue(String paramName, String paramNameOld, String alternateValue, JSONObject _jsonObject) {
        String value = alternateValue;

        String param =  paramName ;

        param = checkParams(param, _jsonObject);

        if (param != null && _jsonObject.has(param))
            try {
                /*Object unidentifiedValue = _jsonObject.get(param);

                if (unidentifiedValue != null && unidentifiedValue instanceof String)
                    value = (String) unidentifiedValue;
                else
                    value = null;*/
                value = _jsonObject.getString(param);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return value;
    }

    public ArrayList<String> getStringArray(
            JSONResultParser resultParser,
            String arrayParam,
            String arrayParamOld,
            String childParam,
            String childParamOld) {
        ArrayList<String> result;

        JSONArray jsonArray =
                resultParser.getJSONArrayValue(arrayParam, arrayParamOld);

        if (jsonArray != null) {
            result = new ArrayList<String>();

            for (int x = 0; x < jsonArray.length(); x++)
                try {
                    result.add(resultParser.getStringValue(childParam, childParamOld, new JSONObject(jsonArray.getString(x))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
        } else {
            result = new ArrayList<String>();

            result.add(resultParser.getStringValue(childParam, childParamOld));
        }


        return result;
    }

    public int getIntValue(String paramName, String paramNameOld) {
        return getIntValue(paramName, paramNameOld, 0, jsonObject);
    }

    public int getIntValue(String paramName, String paramNameOld, JSONObject _jsonObject) {
        return getIntValue(paramName, paramNameOld, 0, _jsonObject);
    }

    public int getIntValue(String paramName, String paramNameOld, int alternateValue) {
        return getIntValue(paramName, paramNameOld, alternateValue, jsonObject);
    }

    public int getIntValue(String paramName, String paramNameOld, int alternateValue, JSONObject _jsonObject) {
        int value = alternateValue;

        String param =  paramName;

        param = checkParams(param, _jsonObject);

        if (param != null && _jsonObject.has(param))
            try {
                /*Object unidentifiedValue = _jsonObject.get(param);

                if (unidentifiedValue != null && unidentifiedValue instanceof Integer)
                    value = (Integer) unidentifiedValue;
                else
                    value = 0;*/
                value = _jsonObject.getInt(param);
            } catch (JSONException e) {
                //e.printStackTrace();
            }

        return value;
    }

    public double getDoubleValue(String paramName, String paramNameOld) {
        return getDoubleValue(paramName, paramNameOld, 0.0D);
    }

    public double getDoubleValue(String paramName, String paramNameOld, double alternateValue) {
        double value = alternateValue;

        String param =  paramName;

        param = checkParams(param, jsonObject);

        if (param != null && jsonObject.has(param))
            try {
                /*Object unidentifiedValue = jsonObject.get(param);

                if (unidentifiedValue != null && unidentifiedValue instanceof Double)
                    value = (Double) unidentifiedValue;
                else
                    value = 0.0D;*/
                value = jsonObject.getDouble(param);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return value;
    }

    public long getLongValue(String paramName, String paramNameOld) {
        return getLongValue(paramName, paramNameOld, 0L);
    }

    public long getLongValue(String paramName, String paramNameOld, long alternateValue) {
        long value = alternateValue;

        String param =  paramName;

        param = checkParams(param, jsonObject);

        if (param != null && jsonObject.has(param))
            try {
                /*Object unidentifiedValue = jsonObject.get(param);

                if (unidentifiedValue != null && unidentifiedValue instanceof Long)
                    value = (Long) unidentifiedValue;
                else
                    value = 0L;*/
                value = jsonObject.getLong(param);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return value;
    }

    public boolean getBoolValue(String paramName, String paramNameOld) {
        return getBoolValue(paramName, paramNameOld, false, jsonObject);
    }

    public boolean getBoolValue(String paramName, String paramNameOld, boolean alternateValue, JSONObject _jsonObject) {
        boolean value = alternateValue;

        String param =  paramName;

        param = checkParams(param, _jsonObject);

        if (param != null && _jsonObject.has(param))
            try {
                String valueString = _jsonObject.getString(param);

                if (valueString.equalsIgnoreCase("true") || valueString.equalsIgnoreCase("1"))
                    value = true;
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return value;
    }

    public boolean isNull(String paramName){

        return jsonObject.isNull(paramName);
    }
}

