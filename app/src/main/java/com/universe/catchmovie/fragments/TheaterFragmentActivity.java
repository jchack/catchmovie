package com.universe.catchmovie.fragments;

/**
 * Created by juancarloslopez on 11/13/15.
 */


import com.universe.catchmovie.Resources.ResourcesReference;
import com.universe.catchmovie.enums.Languages;
import com.universe.catchmovie.interfaces.IAsyncToolTask;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.universe.catchmovie.adapters.MoviesAdapter;
import com.universe.catchmovie.BaseActivity;
import com.universe.catchmovie.MovieResultsActivity;
import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Movie;
import com.universe.catchmovie.utils.MovieTools;

import java.util.ArrayList;

public class TheaterFragmentActivity extends Fragment implements IAsyncToolTask {

    private ExpandableListView expandableListView;

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (((MovieResultsActivity) getActivity()).theatersAdapter != null) {

            expandableListView = new ExpandableListView(getActivity());
            expandableListView.setDividerHeight(2);
            expandableListView.setDivider(new ColorDrawable(Color.GRAY));
            expandableListView.setGroupIndicator(null);
            expandableListView.setClickable(true);

            ((MovieResultsActivity) getActivity()).theatersAdapter.fragmentActivity = this;

            expandableListView.setAdapter(((MovieResultsActivity) getActivity()).theatersAdapter);
        }

        return expandableListView;
    }

    @Override
    public void processFinish(Object output) {
        // Fragments have access to their parent Activity's FragmentManager. You can
        // obtain the FragmentManager like this.
        FragmentManager fm = getFragmentManager();

        if (fm != null) {
            // Perform the FragmentTransaction to load in the list tab content.
            // Using FragmentTransaction#replace will destroy any Fragments
            // currently inside R.id.fragment_content and add the new Fragment
            // in its place.
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.itemlist_layout, new TheaterDetailFragmentActivity()).addToBackStack("back2");
            ft.commit();
        }
    }

    @Override
    public ArrayList<String> processToRun(Object... input) {
        String output;

        try {
            output = (((BaseActivity) getActivity()).webServicesHandler.GetMovieSchedules(input[0].toString(), input[1].toString(), getActivity()));

            if (output != null) {
                MovieTools movieTool =
                        new MovieTools(Languages.ES.toString(),
                                ((BaseActivity) getActivity()).webServicesHandler, getActivity());


                ArrayList<Movie> collection =  movieTool.parseMovieSchedules(output, getActivity());

                if (collection != null) {

                   /* ArrayList<Movie> movieCollection = new ArrayList<>();

                    Movie movie = new Movie();
                    movie.setNombre(getActivity().getString(R.string.textview_movie_header_label));

                    movieCollection.add(movie); // ADD THE HEADER

                    for(Movie _movie_ : collection)
                            movieCollection.add(_movie_);*/

                    ((MovieResultsActivity) getActivity()).theaterSchedulesAdapter =
                            new MoviesAdapter(getActivity(), R.layout.itemlist_movies_layout, collection, ResourcesReference.MovieResource,true);//TheaterSchedulesAdapter(getActivity(), R.id.itemlist_theatername_textView, movieCollection);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}