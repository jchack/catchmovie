package com.universe.catchmovie.fragments;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.dialog.CustomAlertDialog;
import com.universe.catchmovie.interfaces.ICustomAlertDialog;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.universe.catchmovie.MovieDetailsActivity;
import com.universe.catchmovie.MovieResultsActivity;
import com.universe.catchmovie.R;
import com.universe.catchmovie.adapters.ExpandableMoviesAdapter;
import com.universe.catchmovie.adapters.MoviesAdapter;
import com.universe.catchmovie.objects.Movie;
import com.universe.catchmovie.objects.MovieByGenre;
import com.universe.catchmovie.objects.MoviePremiere;
import com.universe.catchmovie.Resources.ResourcesReference;

import java.util.ArrayList;

public class MoviesFragmentActivity extends Fragment implements ICustomAlertDialog, OnClickListener,
        /*IAsyncToolTask,*/ OnItemClickListener {

    public static final String genre_filter = "genre_filter";
    public ExpandableMoviesAdapter eMoviesAdapter;
    //private ExpandableListView expandableListView;
    private ListView listView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments().getBoolean(genre_filter)) {


        } else {
            if (getActivity() != null)
                setData(((MovieResultsActivity) getActivity()).moviePremiere);
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        //if (getActivity().getIntent().getExtras() != null && getActivity().getIntent().hasExtra(genre_filter)) {


        if (getArguments() != null  && getArguments().getBoolean(genre_filter)) {

            ((MovieResultsActivity)getActivity()).changeToMovieByGender();

            ArrayList<MovieByGenre> movieByGenreCollection =
                    (ArrayList<MovieByGenre>) ((MovieResultsActivity)getActivity()).movieByGender; //getActivity().getIntent().getExtras().get(genre_filter);

            eMoviesAdapter = new ExpandableMoviesAdapter(movieByGenreCollection, MovieDetailsActivity.class);

            eMoviesAdapter.setInflater(inflater, getActivity());

            ExpandableListView expandableListView = new ExpandableListView(getActivity());
            expandableListView.setDividerHeight(2);
            expandableListView.setGroupIndicator(null);
            expandableListView.setClickable(true);

            eMoviesAdapter.fragmentActivity = this;

            expandableListView.setAdapter(eMoviesAdapter);

            return expandableListView;
        } else {
            listView = new ListView(getActivity());

            return listView;
        }
    }

    public void setData(MoviePremiere data) {
        if (data != null) {
            ArrayList<Movie> _data = new ArrayList<Movie>();

            for (Movie item : data.getEstrenosArray())
                _data.add(item);
            for (Movie item : data.getCarteleraArray())
                _data.add(item);

            MoviesAdapter adapter =
                    new MoviesAdapter(getActivity(), R.layout.itemlist_movies_layout, _data, ResourcesReference.MovieResource,true);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(this);
            // }
        } else {
            MoviesAdapter adapter = new MoviesAdapter(getActivity(), R.layout.itemlist_movies_layout, null,ResourcesReference.MovieResource, true);
            listView.setAdapter(adapter);

            CustomAlertDialog dialog = new CustomAlertDialog(
                    getActivity(),
                    getString(R.string.dialog_noresults_header),
                    getResources().getString(R.string.dialog_noresults_text),
                    false,
                    true,
                    false,
                    null, R.layout.main_alert_dialog_layout, ResourcesReference.CustomAlertDialogSubViews);
            dialog.delegate = this;
            dialog.show();
        }
    }


    @Override
    public void onOkAlertClick(CustomAlertDialog dialog, Activity callingActivity) {
        if (dialog.isShowing())
            dialog.dismiss();

        callingActivity.finish();
    }

    @Override
    public void onCancelAlertClick(CustomAlertDialog dialog, Activity callingActivity) {
        if (dialog.isShowing())
            dialog.dismiss();
    }


    @Override
    public void onClick(View view) {
        /*AsyncToolTask task = new AsyncToolTask(getActivity(), R.layout.loading_dialog_layout, ResourcesReference.CustomLoadingDialogSubViews);
        task.delegate = this;
        task.execute();*/
    }


    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        if (getActivity() != null) {
            ListAdapter listAdapter = listView.getAdapter();
            Movie movie = (Movie) listAdapter.getItem(position);

            Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
            intent.putExtra(getResources().getString(R.string.movieIDParam), String.valueOf(movie.getMovieId()));

            startActivity(intent);
        }
    }
}