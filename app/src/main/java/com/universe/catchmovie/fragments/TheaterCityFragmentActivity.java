package com.universe.catchmovie.fragments;

/**
 * Created by juancarloslopez on 11/13/15.
 */


import com.universe.catchmovie.enums.Languages;
import com.universe.catchmovie.objects.City;
import com.universe.catchmovie.task.AsyncToolTask;
import com.universe.catchmovie.interfaces.IAsyncToolTask;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.universe.catchmovie.BaseActivity;
import com.universe.catchmovie.MovieResultsActivity;
import com.universe.catchmovie.R;
import com.universe.catchmovie.adapters.ExpandableTheatersAdapter;
import com.universe.catchmovie.objects.Theater;
import com.universe.catchmovie.Resources.ResourcesReference;
import com.universe.catchmovie.utils.MovieTools;

        import java.util.ArrayList;

public class TheaterCityFragmentActivity extends ListFragment implements OnItemClickListener, IAsyncToolTask {

    private City myCity;

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            // Create an instance of the custom adapter for the GridView. A static array of location data
            // is stored in the Application sub-class for this app. This data would normally come
            // from a database or a web service.

            setData();
        }

    }

    public void setData() {
        if (((MovieResultsActivity) getActivity()).theaterCitiesResult != null)
            setListAdapter(((MovieResultsActivity) getActivity()).theaterCitiesResult);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Activity activity = getActivity();

        if (activity != null) {
            ListAdapter listAdapter = getListAdapter();
            myCity = (City) listAdapter.getItem(position);

            AsyncToolTask tool = new AsyncToolTask(activity, R.layout.loading_dialog_layout, ResourcesReference.CustomLoadingDialogSubViews);
            tool.delegate = this;
            tool.execute(myCity);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) { /* LEAVE EMPTY */ }

    @Override
    public void processFinish(Object output) {
        // Fragments have access to their parent Activity's FragmentManager. You can
        // obtain the FragmentManager like this.
        FragmentManager fm = getFragmentManager();

        if (fm != null) {
            // Perform the FragmentTransaction to load in the list tab content.
            // Using FragmentTransaction#replace will destroy any Fragments
            // currently inside R.id.fragment_content and add the new Fragment
            // in its place.
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.itemlist_layout, new TheaterFragmentActivity()).addToBackStack("back1");
            ft.commit();
        }
    }

    @Override
    public Object processToRun(Object... input) {
        String output;

        try {
            output = (((BaseActivity) getActivity()).webServicesHandler.GetTheaterByCity(((City) input[0]).getCode(), getActivity()));

            if (output != null) {
                MovieTools movieTool =
                        new MovieTools(Languages.ES.toString(),
                                ((BaseActivity) getActivity()).webServicesHandler, getActivity());

                ArrayList<Theater> theaters = movieTool.parseTheaterByCity(output, myCity, getActivity());
                ArrayList<Theater> theaterCompany = new ArrayList<Theater>();
                ArrayList<ArrayList<Theater>> theaterCollection = new ArrayList<ArrayList<Theater>>();

                for (Theater innerTheater : theaters) {
                    theaterCompany.add(innerTheater);

                    theaterCollection.add(innerTheater.getCineArray());
                }

                ((MovieResultsActivity) getActivity()).theatersAdapter =
                        new ExpandableTheatersAdapter(theaterCompany, theaterCollection);

                ((MovieResultsActivity) getActivity()).theatersAdapter.setInflater(
                        (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE),
                        getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}