package com.universe.catchmovie.fragments;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.universe.catchmovie.BaseActivity;
import com.universe.catchmovie.MovieDetailsActivity;
import com.universe.catchmovie.MovieResultsActivity;
import com.universe.catchmovie.R;
import com.universe.catchmovie.objects.Movie;

public class TheaterDetailFragmentActivity extends ListFragment implements OnItemClickListener {

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if (getActivity() != null)
            setData();

    }

    public void setData() {
        if (((MovieResultsActivity) getActivity()).theaterSchedulesAdapter != null)
            setListAdapter(((MovieResultsActivity) getActivity()).theaterSchedulesAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Activity activity = getActivity();

        if (activity != null) {
            ListAdapter listAdapter = getListAdapter();

            // if (position != 0) {
            Movie movie = (Movie) listAdapter.getItem(position);

            Intent intent = new Intent(getActivity(), MovieDetailsActivity.class);
            intent.putExtra(getResources().getString(R.string.movieIDParam), String.valueOf(movie.getMovieId()));

            startActivity(intent);
            //}
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) { /* LEAVE EMPTY */ }
}