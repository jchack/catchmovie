package com.universe.catchmovie.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.universe.catchmovie.BaseActivity;
import com.universe.catchmovie.MovieDetailsActivity;
import com.universe.catchmovie.R;
import com.universe.catchmovie.Resources.ResourcesReference;
import com.universe.catchmovie.adapters.MoviesAdapter;
import com.universe.catchmovie.enums.Languages;
import com.universe.catchmovie.interfaces.IMovieTools;
import com.universe.catchmovie.interfaces.OnFragmentInteractionListener;
import com.universe.catchmovie.objects.City;
import com.universe.catchmovie.objects.Movie;
import com.universe.catchmovie.objects.MoviePremiere;
import com.universe.catchmovie.utils.MovieTools;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link HomeMovieSlider#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeMovieSlider extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String mParam_movie_premier = "moviePremier";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View rootView;
    private ObservableScrollView mScrollView;
    private boolean instanceLoaded = false;
    private TextView countTextView;
    private MoviePremiere mMoviePremiere;
    public IMovieTools delegate;
    public TableRow mTableRowThreater;
    public TableRow mTableRowRelease;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeMovieSlider.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeMovieSlider newInstance(MoviePremiere mMoviePremiere, String param1, String param2) {


        HomeMovieSlider fragment = new HomeMovieSlider();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM1, param2);
        args.putParcelable(mParam_movie_premier, mMoviePremiere);

        fragment.setArguments(args);

        return fragment;
    }

    public HomeMovieSlider() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mMoviePremiere = (MoviePremiere) getArguments().getParcelable(mParam_movie_premier);
        }

        LayoutInflater inflater = getActivity().getLayoutInflater();
        rootView = inflater.inflate(R.layout.fragment_home_movie_slider,
                (ViewGroup) getActivity().findViewById(R.id.materialViewPager), false);

        mTableRowThreater = (TableRow) rootView.findViewById(R.id.movies_slider_tableRowThreater);
        mTableRowRelease = (TableRow) rootView.findViewById(R.id.movies_slider_tableRowRelease);

        countTextView = (TextView) rootView.findViewById(R.id.logo_white);
      //  countTextView.setText(mParam1);
        instanceLoaded = true;

        MaterialViewPagerHelper.registerScrollView(getActivity(),mScrollView,null);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d("onCreateView", "fragment 2 created");
        ViewGroup viewGroup = (ViewGroup) rootView.getParent();
        if (viewGroup != null) {
            viewGroup.removeAllViewsInLayout();
        }
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mMoviePremiere != null
                && mTableRowThreater != null
                && mTableRowRelease != null){

            ViewGroup viewGroup =  (ViewGroup) getActivity().findViewById(R.id.materialViewPager);



            MoviesAdapter mMovieAdapterThreater = new MoviesAdapter(
                    getActivity(),0,mMoviePremiere.getCarteleraArray(),ResourcesReference.MovieResourceCover,false);

            MoviesAdapter mMovieAdapterRelease = new
                    MoviesAdapter(getActivity(),0,mMoviePremiere.getEstrenosArray(),ResourcesReference.MovieResourceCover,false);


            for (int i = 0; i < mMovieAdapterThreater.getCount();i++){
                View v = mMovieAdapterThreater.getView(i, null, mTableRowThreater);
                v.setOnClickListener(this);
                mTableRowThreater.addView(v);
            }

            for (int i = 0; i < mMovieAdapterRelease.getCount();i++){
                View v = mMovieAdapterRelease.getView(i, null, mTableRowRelease);
                v.setOnClickListener(this);
                mTableRowRelease.addView(v);
            }


        }

        mScrollView = (ObservableScrollView) view.findViewById(R.id.scrollView);
        MaterialViewPagerHelper.registerScrollView(getActivity(),mScrollView,null);
    }


    @Override
    public void onClick(View v) {
        if(v != null) {

            Intent i = new Intent(getActivity(), MovieDetailsActivity.class);
            i.putExtra(getActivity().getString(R.string.movieIDParam),
                    String.valueOf(((MoviesAdapter.ViewHolder) v.getTag()).moviename.getTag()));

            getActivity().startActivity(i);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }*/

}
