package com.universe.catchmovie.fragments;

/**
 * Created by apple on 11/26/15.
 */
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.universe.catchmovie.R;
import com.universe.catchmovie.adapters.TestRecyclerViewAdapter;
import com.universe.catchmovie.objects.MoviePremiere;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class RecyclerViewFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    private MoviePremiere mMoviePremier;
    /*params*/
    private static String mParam_movie_premier = "mParam_movie_premier";

    private static final int ITEM_COUNT = 1; //100;

    private List<Object> mContentItems = new ArrayList<>();

    public static RecyclerViewFragment newInstance(MoviePremiere mMoviePremiere) {
        RecyclerViewFragment mRecyclerView = new RecyclerViewFragment();
        Bundle args = new Bundle();
        args.putParcelable(mParam_movie_premier, mMoviePremiere);
        mRecyclerView.setArguments(args);

        return mRecyclerView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       if(getArguments() != null){
           mMoviePremier = (MoviePremiere) getArguments().getParcelable(mParam_movie_premier);
       }
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new RecyclerViewMaterialAdapter(new TestRecyclerViewAdapter(mContentItems,getActivity(),mMoviePremier));
        mRecyclerView.setAdapter(mAdapter);

        {
            for (int i = 0; i < ITEM_COUNT; ++i)
                mContentItems.add(new Object());
            mAdapter.notifyDataSetChanged();
        }

        MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);
    }
}
