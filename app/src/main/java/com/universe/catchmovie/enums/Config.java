package com.universe.catchmovie.enums;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public enum Config {



    PRODUCTION("https://paelqa.amarillas.com.do/caribewebapi/api/%s/%s","RD","1"),
    QA("https://paelqa.amarillas.com.do/CaribeWebAPIDevelopment/api/%s/%s","RD","1");


    private String service_url;
    private String business;
    private String siteID;

    Config(String url,String business, String siteID){
        service_url = url;
        this.business = business;

    }

    public String getService_url(){
        return service_url;
    }

    public String getBusiness(){
        return this.business;
    }

    public String getSiteID(){
        return siteID;
    }



}
