package com.universe.catchmovie.enums;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public enum Languages {

    ES("es"),
    EN("en");

    private String lang;

    Languages(String lang){
        this.lang = lang;
    }

    @Override
    public String toString() {
        return this.lang.toString();
    }
}
