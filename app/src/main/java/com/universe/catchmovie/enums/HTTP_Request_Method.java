package com.universe.catchmovie.enums;

/**
 * Created by juancarloslopez on 11/13/15.
 */
public enum HTTP_Request_Method {

    POST("POST"),
    GET("GET"),
    PUT("PUT"),
    DELETE("DELETE"),
    HEAD("HEAD"),
    OPTIONS("OPTIONS");

    private final String value;

    private HTTP_Request_Method(String str){
        this.value = str;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}