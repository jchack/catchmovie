package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.utils.JSONResultParser;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.universe.catchmovie.R;

import java.util.ArrayList;

public class MovieByGenre implements Parcelable {

    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<MovieByGenre> CREATOR = new Parcelable.Creator<MovieByGenre>() {
        public MovieByGenre createFromParcel(Parcel in) {
            return new MovieByGenre(in);
        }

        public MovieByGenre[] newArray(int size) {
            return new MovieByGenre[size];
        }
    };
    private String genero;
    private ArrayList<Movie> peliculas;

    public MovieByGenre() {
    }

    public MovieByGenre(JSONObject jsonObject, Context activity) {
        JSONResultParser resultParser =
                new JSONResultParser(jsonObject);

        genero = resultParser.getStringValue(activity.getString(R.string.response_movie_genre), "cgen");

        if (peliculas == null)
            peliculas = new ArrayList<Movie>();

        JSONArray moviesArray = resultParser.getJSONArrayValue(activity.getString(R.string.response_theater_movies), "mvs");

        if (moviesArray != null)
            for (int i = 0; i < moviesArray.length(); i++)
                try {
                    peliculas.add(new Movie(moviesArray.getJSONObject(i), activity));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
    }

    public MovieByGenre(Parcel parcel) {
        genero = parcel.readString();

        if (peliculas == null)
            peliculas = new ArrayList<Movie>();

        parcel.readTypedList(peliculas, Movie.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeString(genero);
        parcel.writeTypedList(peliculas);
    }

    public ArrayList<Movie> getPeliculas() {
        return peliculas;
    }

    public void setPeliculas(ArrayList<Movie> peliculas) {
        this.peliculas = peliculas;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}

