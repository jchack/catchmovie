package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class MoviePremiere implements Parcelable {
    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<MoviePremiere> CREATOR = new Parcelable.Creator<MoviePremiere>() {
        public MoviePremiere createFromParcel(Parcel in) {
            return new MoviePremiere(in);
        }

        public MoviePremiere[] newArray(int size) {
            return new MoviePremiere[size];
        }
    };
    private ArrayList<Movie> estrenosArray;
    private ArrayList<Movie> carteleraArray;

    //Constructor
    public MoviePremiere() {
        estrenosArray = new ArrayList<Movie>();
        carteleraArray = new ArrayList<Movie>();
    }

    public MoviePremiere(Parcel parcel) {
        if (estrenosArray == null)
            estrenosArray = new ArrayList<Movie>();

        parcel.readTypedList(estrenosArray, Movie.CREATOR);

        if (carteleraArray == null)
            carteleraArray = new ArrayList<Movie>();

        parcel.readTypedList(carteleraArray, Movie.CREATOR);
    }

    public ArrayList<Movie> getEstrenosArray() {
        return estrenosArray;
    }

    public void setEstrenosArray(ArrayList<Movie> estrenosArray) {
        this.estrenosArray = estrenosArray;
    }

    public ArrayList<Movie> getCarteleraArray() {
        return carteleraArray;
    }

    public void setCarteleraArray(ArrayList<Movie> carteleraArray) {
        this.carteleraArray = carteleraArray;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeTypedList(estrenosArray);
        parcel.writeTypedList(carteleraArray);
    }
}

