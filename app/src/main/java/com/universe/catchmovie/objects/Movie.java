package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.utils.JSONResultParser;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.universe.catchmovie.R;

public class Movie implements Parcelable {

    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
    /**
     *
     */
    //private static final long serialVersionUID = 4354364803900773630L;

    private int movieId;
    private boolean estreno;
    private String nombre;
    private String dirigidaPor;
    private String genero;
    private String restriccion;
    private String actores;
    private String argumentos;
    private String trailerLink;
    private String horario;
    private double puntuacion;
    private int cantidadVotantes;
    private String restrintion;

    public Movie() {
    }

    public Movie(JSONObject jsonObject, Context activity) {
        JSONResultParser resultParser =
                new JSONResultParser(jsonObject);

        movieId = resultParser.getIntValue(activity.getString(R.string.response_movie_id), "pid");
        estreno = resultParser.getBoolValue(activity.getString(R.string.response_movie_is_premiere), "pie");
        nombre = resultParser.getStringValue(activity.getString(R.string.response_movie_name) + ",cmv", "pno,cmv");
        dirigidaPor = resultParser.getStringValue(activity.getString(R.string.response_movie_director)+",dp", "dp");
        genero = resultParser.getStringValue(activity.getString(R.string.response_movie_genre)+",ge,gen", "ge,gen");
        restriccion = resultParser.getStringValue(activity.getString(R.string.response_movie_restriction)+"re", "re");
        actores = resultParser.getStringValue(activity.getString(R.string.response_movie_actors)+",ac", "ac");
        argumentos = resultParser.getStringValue(activity.getString(R.string.response_movie_arguments)+",ar", "ar");
        trailerLink = resultParser.getStringValue(activity.getString(R.string.response_movie_trailer)+",tr", "tr");
        // horario = resultParser.getStringValue(activity.getString(R.string.response_movie_schedule)+",hor", "hor");
        puntuacion = resultParser.getDoubleValue(activity.getString(R.string.response_movie_score) + ",pu", "pu");
        cantidadVotantes = resultParser.getIntValue(activity.getString(R.string.response_movie_voters_count) + ",cv", "cv");
        restrintion = resultParser.getStringValue(activity.getString(R.string.response_movie_restriction),"pre");



        JSONArray jsonSchedule  = resultParser.getJSONArrayValue(activity.getString(R.string.response_theater_schedule), "hor");

        if(jsonSchedule != null){
            for(int i=0; i< jsonSchedule.length(); i++){
                try{
                    JSONResultParser scheduleParser =
                            new JSONResultParser(jsonSchedule.getJSONObject(i));

                    horario = scheduleParser.getStringValue(activity.getString(R.string.response_movie_schedule_detail), "trs");
                    String sala = scheduleParser.getStringValue(activity.getString(R.string.response_movie_sala), "tro");

                    horario =
                            String.format(activity.getString(R.string.togglebutton_listingitems_theaters_sin) + ": %s, %s", sala, horario) + "\n" ;

                }catch (JSONException ex){
                    ex.printStackTrace();
                }
            }
        }
    }

    public Movie(Parcel parcel) {
        movieId = parcel.readInt();
        estreno = parcel.readString().equalsIgnoreCase("true");
        nombre = parcel.readString();
        dirigidaPor = parcel.readString();
        genero = parcel.readString();
        restriccion = parcel.readString();
        actores = parcel.readString();
        argumentos = parcel.readString();
        trailerLink = parcel.readString();
        horario = parcel.readString();
        puntuacion = parcel.readDouble();
        cantidadVotantes = parcel.readInt();
        restriccion = parcel.readString();
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public boolean isPremiere() {
        return estreno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirigidaPor() {
        return dirigidaPor;
    }

    public void setDirigidaPor(String dirigidaPor) {
        this.dirigidaPor = dirigidaPor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRestriccion() {
        return restriccion;
    }

    public void setRestriccion(String restriccion) {
        this.restriccion = restriccion;
    }

    public String getActores() {
        return actores;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public String getArgumentos() {
        return argumentos;
    }

    public void setArgumentos(String argumentos) {
        this.argumentos = argumentos;
    }

    public String getTrailerLink() {
        return trailerLink;
    }

    public void setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public double getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(float puntuacion) {
        this.puntuacion = puntuacion;
    }

    public int getCantidadVotantes() {
        return cantidadVotantes;
    }

    public void setCantidadVotantes(int cantidadVotantes) {
        this.cantidadVotantes = cantidadVotantes;
    }

    public void setEstreno(boolean estreno) {
        this.estreno = estreno;
    }

    public String getRestrintion() {
        return restrintion;
    }

    public void setRestrintion(String restrintion) {
        this.restrintion = restrintion;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeInt(movieId);
        parcel.writeString(estreno ? "true" : "false");
        parcel.writeString(nombre);
        parcel.writeString(dirigidaPor);
        parcel.writeString(genero);
        parcel.writeString(restriccion);
        parcel.writeString(actores);
        parcel.writeString(argumentos);
        parcel.writeString(trailerLink);
        parcel.writeString(horario);
        parcel.writeDouble(puntuacion);
        parcel.writeInt(cantidadVotantes);
        parcel.writeString(restrintion);
    }
}