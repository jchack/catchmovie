package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.utils.JSONResultParser;
import com.universe.catchmovie.objects.City;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.universe.catchmovie.R;

import java.util.ArrayList;

public class Theater implements Parcelable {

    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<Theater> CREATOR = new Parcelable.Creator<Theater>() {
        public Theater createFromParcel(Parcel in) {
            return new Theater(in);
        }

        public Theater[] newArray(int size) {
            return new Theater[size];
        }
    };
    /**
     *
     */
    //private static final long serialVersionUID = -5984670124524162655L;

    private int theaterId;
    private String nombre;
    private String horario = "";
    private City ciudad;
    private String direccion;
    private ArrayList<Movie> movieArray;
    private ArrayList<Theater> cineArray;
    private String listadoId;
    private int subscrId;

    public Theater(JSONObject jsonObject, City city, Context activity) {
        JSONResultParser resultParser =
                new JSONResultParser(jsonObject);

        theaterId = resultParser.getIntValue(activity.getString(R.string.response_theater_id), "cid,cgid");
        nombre = resultParser.getStringValue(activity.getString(R.string.response_theater_name), "cnm,cgnm,ctn");
        ciudad = city;
        direccion = resultParser.getStringValue(activity.getString(R.string.response_theater_address), "cdir");
        listadoId = resultParser.getStringValue(activity.getString(R.string.response_listin_id), "lid");
        subscrId = resultParser.getIntValue(activity.getString(R.string.response_customer_id), "sid");


        JSONArray jsonSchedule  = resultParser.getJSONArrayValue(activity.getString(R.string.response_theater_schedule), "hor");


        if(jsonSchedule != null){
            for(int i=0; i< jsonSchedule.length(); i++){
                try{
                    JSONResultParser scheduleParser =
                            new JSONResultParser(jsonSchedule.getJSONObject(i));

                    horario = scheduleParser.getStringValue(activity.getString(R.string.response_movie_schedule_detail), "trs");
                    String sala = scheduleParser.getStringValue(activity.getString(R.string.response_movie_sala), "tro");

                    horario =
                            String.format(activity.getString(R.string.togglebutton_listingitems_theaters_sin) + ": %s, %s", sala, horario) + "\n" ;

                }catch (JSONException ex){
                    ex.printStackTrace();
                }
            }
        }


        movieArray = new ArrayList<Movie>();

        JSONArray jsonArrMovies = resultParser.getJSONArrayValue(activity.getString(R.string.response_theater_movies), "pels");

        if (jsonArrMovies != null)
            for (int a = 0; a < jsonArrMovies.length(); a++)
                try {
                    movieArray.add(new Movie(jsonArrMovies.getJSONObject(a), activity));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        cineArray = new ArrayList<Theater>();

        JSONArray jsonArrTheaters = resultParser.getJSONArrayValue(activity.getString(R.string.response_theater), "cines, ccts");

        if (jsonArrTheaters != null)
            for (int a = 0; a < jsonArrTheaters.length(); a++)
                try {
                    cineArray.add(new Theater(jsonArrTheaters.getJSONObject(a), city, activity));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
    }

    public Theater(Parcel parcel) {
        theaterId = parcel.readInt();
        nombre = parcel.readString();
        horario = parcel.readString();
        ciudad = parcel.readParcelable(City.class.getClassLoader());
        direccion = parcel.readString();

        if (movieArray == null)
            movieArray = new ArrayList<>();

        parcel.readTypedList(movieArray, Movie.CREATOR);

        if (cineArray == null)
            cineArray = new ArrayList<Theater>();

        parcel.readTypedList(cineArray, Theater.CREATOR);

        listadoId = parcel.readString();
        subscrId = parcel.readInt();

    }

    public int getTheaterId() {
        return theaterId;
    }

    public void setTheaterId(int teatherId) {
        this.theaterId = teatherId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public ArrayList<Movie> getMovieArray() {
        return movieArray;
    }

    public void setMovieArray(ArrayList<Movie> movieArray) {
        this.movieArray = movieArray;
    }

    public ArrayList<Theater> getCineArray() {
        return cineArray;
    }

    public void setCineArray(ArrayList<Theater> cineArray) {
        this.cineArray = cineArray;
    }

    public String getListadoId() {
        return listadoId;
    }

    public void setListadoId(String listadoId) {
        this.listadoId = listadoId;
    }

    public int getSubscrId() {
        return subscrId;
    }

    public void setSubscrId(int subscrId) {
        this.subscrId = subscrId;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public City getCiudad() {
        return ciudad;
    }

    public void setCiudad(City ciudad) {
        this.ciudad = ciudad;
    }

    public Theater() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(theaterId);
        parcel.writeString(nombre);
        parcel.writeString(horario);
        parcel.writeParcelable(ciudad, flags);
        parcel.writeString(direccion);
        parcel.writeTypedList(movieArray);
        parcel.writeTypedList(cineArray);
        parcel.writeString(listadoId);
        parcel.writeInt(subscrId);
    }
}

