package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import com.universe.catchmovie.R;

import com.universe.catchmovie.utils.JSONResultParser;
import org.json.JSONObject;

import java.io.Serializable;

public class City implements Parcelable, Serializable {
    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<City> CREATOR = new Parcelable.Creator<City>() {
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        public City[] newArray(int size) {
            return new City[size];
        }
    };
    /**
     *
     */
    private static final long serialVersionUID = -1450250215049487424L;
    String code;
    String name;
    boolean not_city;
    int position;

    public City() {

    }

    public City(String _code, String _name, boolean _notCity, int _position) {
        code = _code;
        name = _name;
        not_city = _notCity;
        position = _position;
    }

    public City(Parcel parcel) {
        code = parcel.readString();
        name = parcel.readString();
        not_city = parcel.readString().equalsIgnoreCase("true");
        position = parcel.readInt();
    }

    public static City createNewCity(JSONObject jsonObject, int _position, Context activity) {
        JSONResultParser resultParser =
                new JSONResultParser(jsonObject);

        return new City(
                Html.fromHtml(resultParser.getStringValue(activity.getResources().getString(R.string.response_city_code), "id,cc","")).toString(),
                Html.fromHtml(resultParser.getStringValue(activity.getResources().getString(R.string.response_city_name), "n,cn","")).toString(),
                resultParser.getBoolValue(activity.getResources().getString(R.string.response_city_is_not_city), "nc"),
                _position);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String p_value) {
        this.code = p_value;
    }

    public String getName() {
        return name;
    }

    public void setName(String p_value) {
        this.name = p_value;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean getNot_city() {
        return not_city;
    }

    public void setNot_city(boolean not_city) {
        this.not_city = not_city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeString(code);
        parcel.writeString(name);
        parcel.writeString(not_city ? "true" : "false");
        parcel.writeInt(position);
    }
}
