package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.utils.JSONResultParser;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;
import com.universe.catchmovie.R;

public class MovieVoteResults implements Parcelable {
    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<MovieVoteResults> CREATOR = new Parcelable.Creator<MovieVoteResults>() {
        public MovieVoteResults createFromParcel(Parcel in) {
            return new MovieVoteResults(in);
        }

        public MovieVoteResults[] newArray(int size) {
            return new MovieVoteResults[size];
        }
    };
    private boolean habiaVotado;
    private int totalVotos;
    private int cantidad;

    public MovieVoteResults() {
    }

    public MovieVoteResults(JSONObject jsonObject, Context activity) {
        JSONResultParser resultParser =
                new JSONResultParser(jsonObject);

        habiaVotado = resultParser.getBoolValue(activity.getString(R.string.response_movie_have_voted), "hvt");
        totalVotos = resultParser.getIntValue(activity.getString(R.string.response_movie_vote_total), "cvt");
        cantidad = resultParser.getIntValue(activity.getString(R.string.response_movie_vote_count), "ppt");
    }

    public MovieVoteResults(Parcel parcel) {
        habiaVotado = parcel.readString().equalsIgnoreCase("true");
        ;
        totalVotos = parcel.readInt();
        cantidad = parcel.readInt();
    }

    public boolean isHabiaVotado() {
        return habiaVotado;
    }

    public void setHabiaVotado(boolean habiaVotado) {
        this.habiaVotado = habiaVotado;
    }

    public int getTotalVotos() {
        return totalVotos;
    }

    public void setTotalVotos(int totalVotos) {
        this.totalVotos = totalVotos;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeString(habiaVotado ? "true" : "false");
        parcel.writeInt(totalVotos);
        parcel.writeInt(cantidad);
    }
}

