package com.universe.catchmovie.objects;

/**
 * Created by juancarloslopez on 11/13/15.
 */


import com.universe.catchmovie.utils.JSONResultParser;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.universe.catchmovie.objects.City;
import com.universe.catchmovie.R;

import java.util.ArrayList;

public class MovieDetails implements Parcelable {
    /**
     * *PARCELABLE METHODS***
     */

    public static final Parcelable.Creator<MovieDetails> CREATOR = new Parcelable.Creator<MovieDetails>() {
        public MovieDetails createFromParcel(Parcel in) {
            return new MovieDetails(in);
        }

        public MovieDetails[] newArray(int size) {
            return new MovieDetails[size];
        }
    };
    private Movie pelicula;
    private ArrayList<Theater> cineArray;
    public MovieDetails() {
        pelicula = new Movie();
        cineArray = new ArrayList<Theater>();
    }

    public MovieDetails(JSONObject jsonObject, Context activity) {
        try {
            JSONResultParser resultParser =
                    new JSONResultParser(jsonObject);

            JSONObject jsonMovie = resultParser.getJSONObjectValue(activity.getString(R.string.response_movie_root),"pe");
            JSONArray jsonTheaterArrayCollection = resultParser.getJSONArrayValue(activity.getString(R.string.response_theater_collection), "trc");

            pelicula = new Movie(jsonMovie, activity);

            if (cineArray == null)
                cineArray = new ArrayList<Theater>();

            if(jsonTheaterArrayCollection.length() > 0) {


                for(int j = 0; j < jsonTheaterArrayCollection.length(); j++){

                    City city = null;

                    if (jsonTheaterArrayCollection.getJSONObject(j).has(activity.getString(R.string.response_theater_city))) {
                        city = new City();
                        city.setName(jsonTheaterArrayCollection.getJSONObject(j)
                                .getString(activity.getString(R.string.response_theater_city)));
                    }

                    JSONResultParser JPARSER =
                            new JSONResultParser(jsonTheaterArrayCollection.getJSONObject(j));

                    JSONArray jsonTheaterArray =
                            JPARSER.getJSONArrayValue(activity.getString(R.string.response_theater), "ttr");


                    if (jsonTheaterArray.length() > 0){
                        for (int i = 0; i < jsonTheaterArray.length(); i++) {
                            try {
                                cineArray.add(new Theater(jsonTheaterArray.getJSONObject(i), city, activity));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public MovieDetails(Parcel parcel) {
        pelicula = parcel.readParcelable(Movie.class.getClassLoader());

        if (cineArray == null)
            cineArray = new ArrayList<Theater>();

        parcel.readTypedList(cineArray, Theater.CREATOR);
    }

    public Movie getPelicula() {
        return pelicula;
    }

    public void setPelicula(Movie pelicula) {
        this.pelicula = pelicula;
    }

    public ArrayList<Theater> getCineArray() {
        return cineArray;
    }

    public void setCineArray(ArrayList<Theater> cineArray) {
        this.cineArray = cineArray;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeParcelable(pelicula, arg1);
        parcel.writeTypedList(cineArray);
    }
}

