package com.universe.catchmovie.constants;

import com.universe.catchmovie.enums.Config;


/**
 * Created by juancarloslopez on 11/13/15.
 */
public class Constants {

    /********************BUSINESS******************/
    public static String BUSINESSUNIT;
    public static String DEVICE;
    public static String SITEID;
    /********************MOVIE******************/
    public static String MOVIE_PREMIERE;
    public static String THEATER_GET_CITIES;
    public static String MOVIE_GET_DATA;
    public static String THEATER_BY_CITY;
    public static String MOVIE_BY_GENRE;
    public static String MOVIE_SCHEDULES;
    public static String MOVIE_SAVE_VOTE;

    /******************PARAMS*********************/
    public static final String mParam_movie_premier = "mParam_movie_premier";


    public static void initialize(Config config, boolean qa){

        BUSINESSUNIT = config.getBusiness();

        DEVICE = "Android_1447420897139";
        SITEID = config.getSiteID();

        MOVIE_PREMIERE =        (String.format(config.getService_url(),"Movies","GetMovies"));
        /***************************************************************************************/
        THEATER_GET_CITIES =    (String.format(config.getService_url(),"Movies","GetMovieCities"));
        MOVIE_GET_DATA =        (String.format(config.getService_url(),"Movies","GetMovieData"));
        THEATER_BY_CITY =       (String.format(config.getService_url(),"Movies","GetTheaterByCity"));
        MOVIE_SCHEDULES =       (String.format(config.getService_url(),"Movies","GetMoviesByTheater"));
        /***************************************************************************************/
        MOVIE_BY_GENRE =        (String.format(config.getService_url(),"Movies","GetMoviesbyGenre"));
        MOVIE_SAVE_VOTE =       (String.format(config.getService_url(),"Movies","SetMovieVote"));

    }
}
