package com.universe.catchmovie;

/**
 * Created by juancarloslopez on 11/13/15.
 */

import com.universe.catchmovie.dialog.CustomAlertDialog;
import com.universe.catchmovie.enums.Languages;
import com.universe.catchmovie.interfaces.ICustomAlertDialog;
import com.universe.catchmovie.constants.Constants;
import com.universe.catchmovie.task.AsyncToolTask;
import com.universe.catchmovie.task.ImageLoader;
import com.universe.catchmovie.interfaces.IAsyncToolTask;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.universe.catchmovie.adapters.MovieSchedulesAdapter;
import com.universe.catchmovie.objects.Movie;
import com.universe.catchmovie.objects.MovieDetails;
import com.universe.catchmovie.objects.Theater;
import com.universe.catchmovie.Resources.ResourcesReference;
import com.universe.catchmovie.R;
import com.universe.catchmovie.utils.MovieTools;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MovieDetailsActivity extends BaseActivity implements IAsyncToolTask, OnClickListener,
        ICustomAlertDialog {

    private RatingBar ratingBar;
    private Movie movie;
    private ToggleButton firstToggleButton;
    private ToggleButton secondToggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        setupActionBar();

        String movieID = getIntent().getStringExtra(getResources().getString(R.string.movieIDParam));

        initComponent();

        AsyncToolTask tool = new AsyncToolTask(this, R.layout.loading_dialog_layout, ResourcesReference.CustomLoadingDialogSubViews);
        tool.delegate = this;
        tool.execute(movieID);
    }


    private void initComponent(){

        firstToggleButton = (ToggleButton) findViewById(R.id.moviedetail_sinopsis_togglebutton);
        secondToggleButton = (ToggleButton) findViewById(R.id.moviedetail_schedule_togglebutton);

        firstToggleButton.setOnClickListener(this);
        secondToggleButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void processFinish(Object output) {
        if (output != null && output instanceof MovieDetails) {
            try {
                MovieDetails movieDetails = (MovieDetails) output;
                movie = movieDetails.getPelicula();

                ImageLoader il = new ImageLoader(this);
                il.DisplayImage("http://ads.amarillas.com.do/cine/movie" + String.valueOf(movie.getMovieId()) + ".jpg", (ImageView) findViewById(R.id.moviedetail_movieimg_imageView));

                ((TextView) findViewById(R.id.moviedetail_name_textView)).setText(movie.getNombre());
                // moviedesc.setText(movie.getArgumentos());

                double rating = movie.getPuntuacion();

                ImageView movierating = (ImageView) findViewById(R.id.moviedetail_ranking_imageView);

                if (rating > 0.0D && rating <= 1.0D)
                    movierating.setImageDrawable(getResources().getDrawable(R.mipmap.ranking_1));
                else if (rating > 1.0D && rating <= 2.0D)
                    movierating.setImageDrawable(getResources().getDrawable(R.mipmap.ranking_2));
                else if (rating > 2.0D && rating <= 3.0D)
                    movierating.setImageDrawable(getResources().getDrawable(R.mipmap.ranking_3));
                else if (rating > 3.0D && rating <= 4.0D)
                    movierating.setImageDrawable(getResources().getDrawable(R.mipmap.ranking_4));
                else if (rating > 4.0D)
                    movierating.setImageDrawable(getResources().getDrawable(R.mipmap.ranking_5));
                else
                    movierating.setImageDrawable(getResources().getDrawable(R.mipmap.ranking_0));

                // ((TextView) findViewById(R.id.moviedetail_rating_votes_textView)).setText(movie.getCantidadVotantes() + ((String) getText(R.string.textview_movie_rate_votes_label)));

                String total = "0";

                try {
                    DecimalFormat formater = new DecimalFormat("#");
                    total = formater.format(Double.valueOf(String.valueOf(movie.getPuntuacion())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ((TextView) findViewById(R.id.moviedetail_ratingtotal_textView)).setText(total + "/5");

                ((TextView) findViewById(R.id.moviedetail_directedby_textView)).setText(movie.getDirigidaPor());
                ((TextView) findViewById(R.id.moviedetail_genre_textView)).setText(movie.getGenero());
                //((TextView) findViewById(R.id.moviedetail_restrictions_textView)).setText((movie.getRestriccion().compareTo("") == 0 ? "N/A" : movie.getRestriccion()));
                ((TextView) findViewById(R.id.moviedetail_actors_textView)).setText(movie.getActores());
                //((TextView) findViewById(R.id.moviedetail_description_textView)).setText(movie.getArgumentos());
                ((TextView) findViewById(R.id.moviedetail_sinopsis_textView)).setText(movie.getArgumentos());
                ((TextView) findViewById(R.id.moviedetail_clasification_textView)).setText(movie.getRestriccion());

                TextView viewtrailer = (TextView) findViewById(R.id.moviedetail_viewtrailer_label_textView);
                //TextView share = (TextView) findViewById(R.id.moviedetail_share_textView);

                viewtrailer.setTag(movie.getTrailerLink());
                //share.setTag(movie);

                viewtrailer.setOnClickListener(this);
                findViewById(R.id.moviedetail_vote_imageButton).setOnClickListener(this);
                //share.setOnClickListener(this);

                ArrayList<Theater> theaterArray = new ArrayList<Theater>();

                for (Theater item : movieDetails.getCineArray())
                    for (Theater subitem : item.getCineArray())
                        theaterArray.add(subitem);

                MovieSchedulesAdapter adapter = new MovieSchedulesAdapter(this, R.id.itemlist_header_textView, movieDetails.getCineArray(),//theaterArray,
                        R.layout.theater_detail_layout, ResourcesReference.MovieSchedulesAdapterSubViews);

                for (int i = 0; i < adapter.getCount(); i++) {
                    View item = adapter.getView(i, null, null);
                    if (item != null)
                        ((LinearLayout) findViewById(R.id.moviedetail_schedule_layout)).addView(item);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private MovieDetails parseMovieData(String json) {
        MovieTools movieTools =
                new MovieTools(Languages.ES.toString(), webServicesHandler, this);

        return movieTools.parseMovieDetails(json, this);
    }

    @Override
    public Object processToRun(Object... input) {
        String result;
        MovieDetails movieDetails = null;

        try {
            result = (webServicesHandler.GetMovieData(input[0].toString(), this));

            if (result != null)
                movieDetails = parseMovieData(result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return movieDetails;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.moviedetail_viewtrailer_label_textView) {
            Intent intent =
                    new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("vnd.youtube://" + view.getTag().toString().split("v=")[1]));

            startActivity(intent);
        } else if (view.getId() == R.id.moviedetail_vote_imageButton) {
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);

            View layout = inflater.inflate(R.layout.ratemovie_layout, null, false);

            CustomAlertDialog dialog = new CustomAlertDialog(
                    this,
                    null,
                    null,
                    false,
                    true,
                    true,
                    layout, R.layout.main_alert_dialog_layout, ResourcesReference.CustomAlertDialogSubViews);
            dialog.delegate = this;

            if (layout != null)
                ratingBar = (RatingBar) layout.findViewById(R.id.ratethismovie_ratingBar);

            dialog.show();
        }else if(view.getId() == R.id.moviedetail_sinopsis_togglebutton){


            if(!firstToggleButton.isChecked())
                firstToggleButton.setChecked(true);

            changeColorToggle(
                    firstToggleButton,
                    secondToggleButton,
                    null
            );

            changeToggleState(
                    firstToggleButton,
                    secondToggleButton,
                    null);

            showSipnosis();
        }else if(view.getId() == R.id.moviedetail_schedule_togglebutton){


            if(!secondToggleButton.isChecked())
                secondToggleButton.setChecked(true);

            changeColorToggle(
                    secondToggleButton,
                    firstToggleButton,
                    null
            );
            changeToggleState(
                    secondToggleButton,
                    firstToggleButton,
                    null);

            showSchedules();
        }
    }

    @Override
    public void onOkAlertClick(CustomAlertDialog dialog, Activity callingActivity) {
        String rate = String.valueOf(ratingBar.getProgress());

    }

    @Override
    public void onCancelAlertClick(CustomAlertDialog dialog, Activity callingActivity) {
        if (dialog.isShowing())
            dialog.dismiss();
    }

    private void showSchedules(){
        swithcView(R.id.moviedetail_schedule_layout,
                R.id.moviedetail_sinopsis_textView);
    }

    private void showSipnosis(){
        swithcView(R.id.moviedetail_sinopsis_textView,
                R.id.moviedetail_schedule_layout);
    }

    private void swithcView(int showView, int hiddenView){

        if(showView == 0 && hiddenView == 0)
            return;

        findViewById(showView).setVisibility(View.VISIBLE);
        findViewById(hiddenView).setVisibility(View.GONE);
    }

}

